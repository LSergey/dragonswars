<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.10.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Resources/tp.plist</filename>
            </struct>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../Resources/TexturePacker/cloud1/cloud11.png</key>
            <key type="filename">../Resources/TexturePacker/cloud2/cloud21.png</key>
            <key type="filename">../Resources/TexturePacker/cloud3/cloud31.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>124,66,247,132</rect>
                <key>scale9Paddings</key>
                <rect>124,66,247,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/fireBreath01/fireBreath011.png</key>
            <key type="filename">../Resources/TexturePacker/frostBreath01/frostBreath011.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,9,126,17</rect>
                <key>scale9Paddings</key>
                <rect>63,9,126,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/fireBreath02/fireBreath021.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,17,126,34</rect>
                <key>scale9Paddings</key>
                <rect>63,17,126,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/fireBreath03/fireBreath031.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,26,126,52</rect>
                <key>scale9Paddings</key>
                <rect>63,26,126,52</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/fireBreath04/fireBreath041.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,35,126,69</rect>
                <key>scale9Paddings</key>
                <rect>63,35,126,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/fireBreath05/fireBreath051.png</key>
            <key type="filename">../Resources/TexturePacker/frostBreath05/frostBreath051.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,44,126,87</rect>
                <key>scale9Paddings</key>
                <rect>63,44,126,87</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/fireBreath06/fireBreath061.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,52,126,105</rect>
                <key>scale9Paddings</key>
                <rect>63,52,126,105</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/fireBreath07/fireBreath071.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,61,126,122</rect>
                <key>scale9Paddings</key>
                <rect>63,61,126,122</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/fireBreath08/fireBreath081.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,70,126,139</rect>
                <key>scale9Paddings</key>
                <rect>63,70,126,139</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/fireBreath09/fireBreath091.png</key>
            <key type="filename">../Resources/TexturePacker/frostBreath09/frostBreath091.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,79,126,157</rect>
                <key>scale9Paddings</key>
                <rect>63,79,126,157</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/fireBreath10/fireBreath101.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,88,126,177</rect>
                <key>scale9Paddings</key>
                <rect>63,88,126,177</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/frostBreath02/frostBreath021.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,18,126,35</rect>
                <key>scale9Paddings</key>
                <rect>63,18,126,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/frostBreath03/frostBreath031.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,26,126,53</rect>
                <key>scale9Paddings</key>
                <rect>63,26,126,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/frostBreath04/frostBreath041.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,35,126,70</rect>
                <key>scale9Paddings</key>
                <rect>63,35,126,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/frostBreath06/frostBreath061.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,53,126,105</rect>
                <key>scale9Paddings</key>
                <rect>63,53,126,105</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/frostBreath07/frostBreath071.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,61,126,123</rect>
                <key>scale9Paddings</key>
                <rect>63,61,126,123</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/frostBreath08/frostBreath081.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,70,126,140</rect>
                <key>scale9Paddings</key>
                <rect>63,70,126,140</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/frostBreath10/frostBreath101.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,87,126,173</rect>
                <key>scale9Paddings</key>
                <rect>63,87,126,173</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Resources/TexturePacker/redScreenSplash/redSplash.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>360,750,720,1500</rect>
                <key>scale9Paddings</key>
                <rect>360,750,720,1500</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../Resources/TexturePacker</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array>
            <string>try-pro-features</string>
            <string>cocos2d-version-for-polygon</string>
        </array>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
