#pragma once

#include "cocos2d.h"

using namespace cocos2d;

class RTDataHelper
{
public:
    static RTDataHelper& instance();

    Vec2 convertToServerPoint(const Vec2& point, Vec2 bgPos);
    Vec2 convertFromServerPoint(const Vec2& point, Vec2 bgPos);

private:
    RTDataHelper() = default;
    ~RTDataHelper() = default;
};
