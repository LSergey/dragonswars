#include "EnterNicknameScene.h"
#include "MainScene.h"
#include "AppDelegate.h"
#include "GSMainManager.h"
#include "reader/CreatorReader.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include <android/log.h>
#endif

using namespace cocos2d;

cocos2d::Scene* EnterNicknameScene::createScene()
{
    EnterNicknameScene *pRet = new(std::nothrow) EnterNicknameScene();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

bool EnterNicknameScene::init()
{
    if ( !Scene::init() )
    {
        return false;
    }
    
    _editbox = static_cast<EditBox*>(utils::findChild(_currentScene, "editbox"));
    _editbox->setPlaceHolder("Enter Nickname...");
    _editbox->setMaxLength(15);
    _editbox->setInputMode(EditBox::InputMode::SINGLE_LINE);
    _editbox->setReturnType(EditBox::KeyboardReturnType::DEFAULT);
    _editbox->setDelegate(this);

    auto doneBtn = utils::findChild<ui::Button*>(_currentScene, "doneBtn");
    doneBtn->addClickEventListener([this](Ref*) {
        GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
        request.SetEventKey("setPlayerNickname");
        std::string text(_editbox->getText());
        request.SetEventAttribute("nick", text);

        std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
                std::bind(&EnterNicknameScene::requestUseResponse, this, std::placeholders::_1, std::placeholders::_2);
        request.Send(callback, 0);
    });

    return true;
}

void EnterNicknameScene::editBoxReturn(EditBox *editBox)
{
    if (editBox)
    {
        std::string text(editBox->getText());
        if (text.size() > editBox->getMaxLength())
        {
            auto newStr = text.substr(0, editBox->getMaxLength());
            editBox->setText(newStr.c_str());
        }
    }
}

void EnterNicknameScene::requestUseResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response)
{
    if (response.GetHasErrors())
    {
    }
    else
    {
        AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->requestPlayerData();
        Director::getInstance()->replaceScene(MainScene::createScene());
    }
}

EnterNicknameScene::EnterNicknameScene()
{
    auto reader = creator::CreatorReader::createWithFilename("creator/EnterNameScene.ccreator");
    reader->setup();
    _currentScene = reader->getSceneGraph();
    addChild(_currentScene);
}
