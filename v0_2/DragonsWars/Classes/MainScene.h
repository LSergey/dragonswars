#pragma once

#include "cocos2d.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class MainScene : public cocos2d::Scene
{
public:

    static Scene *createScene();
    
    virtual bool init();
    virtual void update(float dt);

    Scene *getScene() const;
    
protected:
    enum DescriptionType {exp = 0, coin, wins, kills};
    MainScene();
    ~MainScene();

    void setupPlayerData();
    void setupIconsDescriptions();
    void setDescriptionText(DescriptionType type);

    Scene* _currentScene = nullptr;
    Layer* _scenesLayer = nullptr;
    EventListenerTouchOneByOne* listener = nullptr;

    bool _firstSetup = true;
};
