#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

#include <GameSparks/generated/GSRequests.h>
#include <GameSparks/generated/GSMessages.h>

using namespace GameSparks::Api::Messages;

using namespace cocos2d;
using namespace ui;

class PlayerObject;
class SneakyJoystickSkinnedBase;
class Bullet;
class AnimatedItemWithCollision;
class SpellSlot;

enum GameMode {oneXone = 0, twoXtwo};

class BaseGameScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene(GameMode mode);

    virtual bool init();
    
    virtual void updateOpponentPosition(Vec2, float degree, int peerId);
    virtual void opponentFireBullet(int peerId, Vec2 pos, int degree, int bulletType, int bulletLevel);
    virtual void anotherPlayerStatusChanged(int peerId, int isDie);

    void requestBattleDataResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response);
    void spawnCloud(int type, int yPos, int xSide, int moveToTime, std::string id);

protected:
    virtual void update(float dt);
    
    virtual void additionalElementsUpdate();
    virtual void initTopElementsLine();
    virtual void initPlayersPos();
    virtual void initStaticElements();
    
    virtual void updatePlayerPos();
    virtual void sendPlayerPos(float);
    virtual void updateMatchTimer(float);
    virtual void updateBullets(float);
    virtual void updateStaticPlayersPos();
    virtual void updatePreparationLabel(float);
    virtual void updateScore();
    virtual void updateRedSplash();
    virtual void updatePlayerHealth();

    bool onContactBegin(PhysicsContact& contact);

    Scene *getScene() const;

    void throwFireball(Ref *pSender, ui::Widget::TouchEventType type);
    void throwFrostball(Ref *pSender, ui::Widget::TouchEventType type);
    void releaseDragonthBreath(Ref *pSender, ui::Widget::TouchEventType type);

    Vector<SpriteFrame*> getAnimation(const char *format, int count);

    void requestBattleDataChange(std::string type, int amount);
    void activateRedSplash();

    
protected:
    int _secondsLeft = 60;
    int _preparationTime = 5;

    PlayerObject* _currentPlayer = nullptr;
    std::vector<PlayerObject*> _otherPlayers;
    SneakyJoystickSkinnedBase* _joystickBase = nullptr;
    std::vector<Bullet*> _myBullets;
    std::vector<Bullet*> _opponentsBullets;
    std::vector<Vec2> _otherPlayerPositions;
    Sprite* _bg;
    Scene* _currentScene;
    LoadingBar* _playerHealthBar = nullptr;
    std::vector<AnimatedItemWithCollision*> _mapAnimatedItesmWithCollision;
    Layer* _scenesLayer = nullptr;

    bool _isPreparation = true;
    bool _battleEnded = false;
    std::pair<int, int> _battleScore;
    std::pair<int, int> _teamsDD;
    SpellSlot* spellSlot1 = nullptr;
    SpellSlot* spellSlot2 = nullptr;
    SpellSlot* spellSlot3 = nullptr;

    Sprite* _redSplash = nullptr;

    GameMode _gameMode;
    Vec2 _bgStartPos;
    float _shitToBorderValue;

private:
    BaseGameScene(GameMode mode);
    ~BaseGameScene();
};
