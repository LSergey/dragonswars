#include "ParamsManager.h"
#include "PlayerDataManager.h"

std::vector<int> fireballDamage = {15, 20, 20, 20, 25, 25, 25, 30, 30, 30, 35,
                                  35, 35, 40, 40, 40, 45, 45, 45, 50};
std::vector<int> frostballDamage = {10, 14, 14, 14, 18, 18, 18, 22, 22, 22, 26,
                                    26, 26, 30, 30, 30, 34, 34, 34, 38};
std::vector<int> breathDamage = {1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5,
                                 5, 5, 6, 6, 6, 7, 7, 7, 8};

std::vector<float> fireballCooldawn = {3.0, 3.0, 2.9, 2.9, 2.9, 2.8, 2.8, 2.8, 2.7, 2.7, 2.7,
                                       2.6, 2.6, 2.6, 2.5, 2.5, 2.5, 2.4, 2.4, 2.4};
std::vector<float> frostballCooldawn = {3.0, 3.0, 2.9, 2.9, 2.9, 2.8, 2.8, 2.8, 2.7, 2.7, 2.7,
                                        2.6, 2.6, 2.6, 2.5, 2.5, 2.5, 2.4, 2.4, 2.4};
std::vector<float> breathCooldawn = {5.0, 5.0, 4.9, 4.9, 4.9, 4.8, 4.8, 4.8, 4.7, 4.7, 4.7,
                                     4.6, 4.6, 4.6, 4.5, 4.5, 4.5, 4.4, 4.4, 4.4};

std::vector<float> fireballAditional = {0, 0, 0, 0, 0, 0, 0,  0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0,  0, 0, 0};
std::vector<float> frostballAditional = {5, 5, 5, 5, 5, 7, 7, 7, 7, 7, 7,
                                         9, 9, 9, 9, 9, 11, 11, 11, 11, 11};
std::vector<float> breathAditional = {0, 0, 0, 0, 0, 0, 0,  0, 0, 0,
                                       0, 0, 0, 0, 0, 0, 0,  0, 0, 0};

std::vector<float> fireballLifetime = {2.0, 2.0, 2.0, 2.1, 2.1, 2.1, 2.2, 2.2, 2.2, 2.3, 2.3,
                                       2.3, 2.4, 2.4, 2.4, 2.5, 2.5, 2.5, 2.6, 2.6};
std::vector<float> frostballLifetime = {2.0, 2.0, 2.0, 2.1, 2.1, 2.1, 2.2, 2.2, 2.2, 2.3, 2.3,
                                        2.3, 2.4, 2.4, 2.4, 2.5, 2.5, 2.5, 2.6, 2.6};
std::vector<float> breathLifetime = {2.0, 2.0, 2.0, 2.1, 2.1, 2.1, 2.2, 2.2, 2.2, 2.3, 2.3,
                                     2.3, 2.4, 2.4, 2.4, 2.5, 2.5, 2.5, 2.6, 2.6};

std::vector<int> fireballUpgradeCost = {5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55,
                                        60, 65, 70, 75, 80, 85, 90, 95, 10};
std::vector<int> frostballUpgradeCost = {5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55,
                                         60, 65, 70, 75, 80, 85, 90, 95, 10};
std::vector<int> breathUpgradeCost = {5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55,
                                      60, 65, 70, 75, 80, 85, 90, 95, 10};

std::vector<int> fireballUpgradeExp = {10, 20, 40, 70, 110, 160, 220, 290, 370, 460, 560,
                                        670, 790, 920, 1060, 1210, 1370, 1540, 1720, 1910};
std::vector<int> frostballUpgradeExp = {10, 20, 40, 70, 110, 160, 220, 290, 370, 460, 560,
                                        670, 790, 920, 1060, 1210, 1370, 1540, 1720, 1910};
std::vector<int> breathUpgradeExp = {10, 20, 40, 70, 110, 160, 220, 290, 370, 460, 560,
                                     670, 790, 920, 1060, 1210, 1370, 1540, 1720, 1910};

ParamsManager* ParamsManager::_instance = nullptr;

ParamsManager *ParamsManager::instance()
{
    if (_instance == nullptr)
        {
            _instance = new ParamsManager();
        }

    return _instance;
}

std::tuple<int, float, float, float> ParamsManager::getSpellBattleParams(PlayerData *playerData, BulletType spellType)
{
    int fireballLevel = playerData->fireball_level - 1;
    int frostballLevel = playerData->frostball_level - 1;
    int breathLevel = playerData->dragon_breath_level - 1;
    switch (spellType)
    {
    case BulletType::fireball :
        return std::make_tuple(fireballDamage[fireballLevel], fireballCooldawn[fireballLevel],
                               fireballAditional[fireballLevel], fireballLifetime[fireballLevel]);
    case BulletType::frostball :
        return std::make_tuple(frostballDamage[frostballLevel], frostballCooldawn[frostballLevel],
                               frostballAditional[frostballLevel], frostballLifetime[frostballLevel]);
    case BulletType::dragon_breath :
        return std::make_tuple(breathDamage[breathLevel], breathCooldawn[breathLevel],
                               breathAditional[breathLevel], breathLifetime[breathLevel]);
    }
}

std::tuple<int, float, float, float> ParamsManager::getSpellParams(PlayerData *playerData, BulletType spellType, int level)
{
    int index = level -1;
    switch (spellType)
    {
    case BulletType::fireball :
        return std::make_tuple(fireballDamage[index], fireballCooldawn[index],
                               fireballAditional[index], fireballLifetime[index]);
    case BulletType::frostball :
        return std::make_tuple(frostballDamage[index], frostballCooldawn[index],
                               frostballAditional[index], frostballLifetime[index]);
    case BulletType::dragon_breath :
        return std::make_tuple(breathDamage[index], breathCooldawn[index],
                               breathAditional[index], breathLifetime[index]);
    }
}

std::tuple<int, int> ParamsManager::getSpellUpgradeParams(PlayerData *playerData, BulletType spellType)
{
    int fireballLevel = playerData->fireball_level - 1;
    int frostballLevel = playerData->frostball_level - 1;
    int breathLevel = playerData->dragon_breath_level - 1;
    switch (spellType)
    {
    case BulletType::fireball :
        return std::make_tuple(fireballUpgradeCost[fireballLevel], fireballUpgradeExp[fireballLevel]);
    case BulletType::frostball :
        return std::make_tuple(frostballUpgradeCost[frostballLevel], frostballUpgradeExp[frostballLevel]);
    case BulletType::dragon_breath :
        return std::make_tuple(breathUpgradeCost[breathLevel], breathUpgradeExp[breathLevel]);
    }
}
