#include "GSAuthManager.h"
#include "AppDelegate.h"

#include <functional>

#include "GameSparks/GS.h"

GSAuthManager::GSAuthManager()
{
}

void GSAuthManager::authWithDeviceIdRequest()
{
    // send a device authentication request with a random device id.
    // this is done so that you can start two instances on the same machine.
    // In a production title, you'd use IGSPlatform::GetDeviceId() instead.
    GameSparks::Api::Requests::DeviceAuthenticationRequest authRequest(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
    std::srand(std::time(nullptr));
    std::stringstream ss;
    ss << std::rand();
    authRequest.SetDeviceId(ss.str() /*"101"*/); // generate a random device id (for easy testing)
    authRequest.SetDeviceOS("Linux");

    std::function<void(GS &, const GameSparks::Api::Responses::AuthenticationResponse) > callback =
            std::bind(&GSAuthManager::authWithIDResponce, this, std::placeholders::_1, std::placeholders::_2);
    authRequest.Send(callback);
}

void GSAuthManager::authWithIDResponce(GS &gsInstance, const GameSparks::Api::Responses::AuthenticationResponse &response)
{
    if (response.GetHasErrors())
    {
        _isAuthenticated = false;
        cocos2d::log("something went wrong during the authentication");
        cocos2d::log("%s", response.GetErrors().GetValue().GetJSON().c_str());
    }
    else
    {
        _isAuthenticated = true;
        _isNewUser = response.GetBaseData().GetBoolean("newPlayer").GetValue();

        cocos2d::log("you successfully authenticated to GameSparks with your credentials");
        cocos2d::log("your displayname is %s.", response.GetBaseData().GetString("displayName").GetValue().c_str());
    }
}

bool GSAuthManager::isAuthenticated() const
{
    return _isAuthenticated;
}

bool GSAuthManager::isNewUser() const
{
    return _isNewUser;
}
