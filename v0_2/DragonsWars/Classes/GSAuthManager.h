#pragma once

#include <memory>

#include "cocos2d.h"

#include <GameSparks/generated/GSRequests.h>
#include <GameSparks/generated/GSMessages.h>

class GSAuthManager
{
public:
    GSAuthManager();
    ~GSAuthManager() = default;

    void authWithDeviceIdRequest();
    void authWithIDResponce(GameSparks::Core::GS& gsInstance,
                            const GameSparks::Api::Responses::AuthenticationResponse& response);

    bool isAuthenticated() const;
    bool isNewUser() const;

private:
    bool _isAuthenticated = false;
    bool _isNewUser = false;

};
