#include "GSMainManager.h"

#include <functional>

GSMainManager::GSMainManager()
    :_platform("R350037XD36f", "nfiMm6pWbsTxapDbiMysP9uNWU8XBibA", true, true)
{
    m_gs = std::make_shared< GameSparks::Core::GS >();
    m_gs->Initialise(&_platform);
}

void GSMainManager::init()
{
    std::function<void(GS &, bool) > onAvailableCallback =
            std::bind(&GSMainManager::onGameSparksAvailable, this, std::placeholders::_1, std::placeholders::_2);
    m_gs->GameSparksAvailable = onAvailableCallback;

    _authManager = std::make_unique<GSAuthManager>();
    m_rtSession = std::make_shared<GameSession>();
    _matchManager = std::make_unique<GSMatchManager>();
    _playerDataManager = std::make_unique<PlayerDataManager>();
}

void GSMainManager::onGameSparksAvailable(GS &gsInstance, bool available)
{
    cocos2d::log("GameSparks is %s.", (available ? "available" : "not available"));

    _authManager->authWithDeviceIdRequest();
    m_status = GSStatus::authanticating;
}

void GSMainManager::update(float dt)
{
    m_gs->Update(dt);
    if (m_status == GSStatus::not_available || m_status == GSStatus::authanticating)
    {
        if (_authManager->isAuthenticated() && _authManager->isNewUser())
        {
            m_status = GSStatus::authenticated_new;
        }

        if (_authManager->isAuthenticated() && !_authManager->isNewUser())
        {
            m_status = GSStatus::authenticated_old;
        }
    }

    if (m_rtSession->Session) {
        m_rtSession->Session->Update();
    }
}

GSStatus GSMainManager::getStatus() const
{
    return m_status;
}

std::unique_ptr<PlayerDataManager>& GSMainManager::getPlayerDataManager()
{
    return _playerDataManager;
}

std::unique_ptr<GSMatchManager> &GSMainManager::getMatchManager()
{
    return _matchManager;
}
