#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

#include <GameSparks/generated/GSRequests.h>
#include <GameSparks/generated/GSMessages.h>

using namespace GameSparks::Api::Messages;

using namespace cocos2d;
using namespace cocos2d::ui;

class SkillsScene : public cocos2d::Scene
{
public:

    static Scene* createScene();
    
    virtual bool init();

    void requestUpgradeResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response);
    void requestUpgradeExpResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response);
    
protected:
    SkillsScene();
    ~SkillsScene() = default;

    virtual void update(float dt);

    void setupFireballUpgrade();
    void setupFrostballUpgrade();
    void setupDragonthBreathUpgrade();

    void upgradeFireballPressed(Ref* pSender, ui::Widget::TouchEventType type);
    void upgradeFrostballPressed(Ref* pSender, ui::Widget::TouchEventType type);
    void upgradeBreathPressed(Ref* pSender, ui::Widget::TouchEventType type);

    Scene* _currentScene = nullptr;
};
