#pragma once

#include "cocos2d.h"
#include "PlayerObject.h"

using namespace cocos2d;

enum BulletType { fireball = 0, frostball, dragon_breath};

class PlayerData;
class PlayerObject;
class Bullet: public Sprite
{
public:
    static Bullet* createObject(BulletType type, int bulletLevel, PlayerObject* connectedObject,
                                Sprite* bg);

    void updatePosition();

    PlayerData *getPlayerData() const;
    PlayerTeamType getPlayerTeamType() const;
    BulletType getBulletType() const;
    int getBulletDamage() const;
    int getBulletLevel() const;
    void setDefaultValues();
    void setPlayerTeamType(PlayerTeamType teamType);
    float getLifetime() const;
    bool isLifetimeEnded() const;

    void exposeBreath(float dt);
    void bulletLifetimeEnd(float dt);
private:
    Bullet(BulletType type, int bulletLevel, PlayerObject* connectedObject, Sprite* bg);
    ~Bullet() = default;

    int _bulletDamage;
    int _bulletLevel;
    float _speedCoeficient = 5.f;
    PlayerTeamType _teamType;
    BulletType _bulletType;
    int breathStateNum = 0;
    float _lifetime;
    bool _lifetimeEnded = false;
    PlayerObject* _connectedObject = nullptr;
    Sprite* _bg;
};
