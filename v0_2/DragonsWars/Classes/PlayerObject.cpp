#include "PlayerObject.h"
#include "stdlib.h"
#include "reader/CreatorReader.h"
#include "PhysicsShapeCache.h"
#include "RTDataHelper.h"
#include "PlayerDataManager.h"
#include "Helpers.h"
#include "SmalMapDot.h"
#include "Bullet.h"
#include "ReceivedDamageSprite.h"
#include "AppDelegate.h"

USING_NS_CC;
static const float EPSILON = 0.00001 ;

PlayerObject* PlayerObject::createObject(Vec2 spawnGlobalPos, PlayerTeamType teamType,
                                         PlayerData *playerData, Sprite* bg, SmalMapDot* dot)
{
    auto reader = creator::CreatorReader::createWithFilename("creator/DragonsScene.ccreator");
    reader->setup();
    auto scene = reader->getSceneGraph(true);

    auto dragon = new PlayerObject(spawnGlobalPos, teamType, playerData, bg, dot);

    //std::string imagePath = "res/dragon" + helpers::to_string(playerData->skin_number) + ".png";
    std::string imagePath, bodyName;
    if (teamType == PlayerTeamType::fire_team)
    {
        imagePath = "res/fireDragon" + helpers::to_string(playerData->skin_number) + ".png";
        bodyName = "fireDragon";
    }
    else
    {
        imagePath = "res/frostDragon" + helpers::to_string(playerData->skin_number) + ".png";
        bodyName = "frostDragon";
    }
    if (dragon && dragon->init() && dragon->initWithFile(imagePath))
    {
        dragon->autorelease();
        dragon->setContentSize(utils::findChild(scene, "fireDragon")->getContentSize());
        dragon->setSpellsPos(Vec2(104, 140),
                             Vec2(-104, 140),
                             Vec2(0, 192));

        PhysicsBody* body = PhysicsShapeCache::getInstance()->createBodyWithName(bodyName);
        if (!body)
        {
            PhysicsShapeCache::getInstance()->addShapesWithFile("physics.plist", dragon->getScale());
            body = PhysicsShapeCache::getInstance()->createBodyWithName(bodyName);
            CC_ASSERT(body != nullptr);
        }

        dragon->setPhysicsBody(body);
        dragon->setDefaultValues();
        dragon->schedule(schedule_selector(PlayerObject::updateCanBeDamagedObjects), 0.1f);

        return dragon;
    }

    CC_SAFE_DELETE(dragon);
    return nullptr;
}

bool PlayerObject::init()
{
    this->scheduleUpdate();

    return true;
}

void PlayerObject::update(float dt)
{
    if (_isAfterRespawn)
    {
        setRotation(_startRotationAngle);
        setDefaultValues();
        updateAfterRespawn();
        return;
    }
}

void PlayerObject::respawn(bool withBgShift)
{
    if (withBgShift)
        _bg->setPosition(_startBgPos);
    setPosition(_spawnGlobalPos);
    setRotation(_startRotationAngle);
    setDefaultValues();

    _dot->updatePositionFromPlayerPosition(this->getPosition(), _bg->getContentSize());
    _health = _playerData->health;
    _startMove = false;
    _isAfterRespawn = true;
    this->scheduleOnce(schedule_selector(PlayerObject::respawnFinished), 5.0f);
}

void PlayerObject::setDefaultValues()
{
    if (_teamType == PlayerTeamType::fire_team)
        _prevAngleDegree = _startRotationAngle + 270;
    else
        _prevAngleDegree = _startRotationAngle - 90;

    auto visibleSize = Director::getInstance()->getWinSize();
    Size default_size(1536, 2046);
    _distanceCoeficient = 10.f * (visibleSize.width / default_size.width);

    setRotation(_startRotationAngle);
}

//TODO refactor it
void PlayerObject::updatePositionWithBgShift(float angle)
{
    if (_isAfterRespawn)
        return;

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    float joystic_rotation = angle;
    float new_angle;
    if (abs(joystic_rotation) > EPSILON)
    {
        float diff;

        if (abs(joystic_rotation - _prevAngleDegree) > 10.f)
        {
            diff = 10.f;
            float direction_of_move = 1; // 1 clockwise, -1 counterclock-wise

            if (joystic_rotation - _prevAngleDegree > 0 && joystic_rotation - _prevAngleDegree < 180)
                direction_of_move = 1;
            else if (joystic_rotation - _prevAngleDegree > 180)
                direction_of_move = -1;
            else if (joystic_rotation - _prevAngleDegree < 0 && joystic_rotation - _prevAngleDegree > -180)
                direction_of_move = -1;
            else
                direction_of_move = 1;

            diff *= direction_of_move;

            new_angle =  _prevAngleDegree + diff + 270;
            if (_prevAngleDegree + diff < 0)
                _prevAngleDegree = 360 + _prevAngleDegree + diff;
            else if (_prevAngleDegree + diff > 360)
                _prevAngleDegree = _prevAngleDegree + diff - 360;
            else
                _prevAngleDegree = _prevAngleDegree + diff;
        }
        else
        {
            new_angle =  joystic_rotation + 270;
            _prevAngleDegree = joystic_rotation;
        }
    }
    else if (abs(_prevAngleDegree) > EPSILON)
        new_angle = _prevAngleDegree + 270;
    else
        new_angle = 0.0;

    if (!(abs(joystic_rotation) > EPSILON) && !_startMove)
        return;
    if (!(abs(joystic_rotation) > EPSILON || abs(helpers::degree_to_radians(_prevAngleDegree + 270)) > EPSILON))
        return;
    this->setRotation(-new_angle);

    _startMove = true;
    float x1, y1;

    x1 = this->getPosition().x + cos(helpers::degree_to_radians(_prevAngleDegree)) * _distanceCoeficient;
    y1 = this->getPosition().y + sin(helpers::degree_to_radians(_prevAngleDegree)) * _distanceCoeficient;

    auto current = RTDataHelper::instance().convertFromServerPoint(this->getPosition(), _bg->getPosition());
    auto future = RTDataHelper::instance().convertFromServerPoint(Vec2(x1, y1), _bg->getPosition());

    x1 = future.x;
    y1 = future.y;

    if (_bg)
    {
        float offset = this->getContentSize().width / 2;
        float offsetX = visibleSize.width / 2;
        float offsetY = visibleSize.height / 2;
        int shiftX = x1 - current.x;
        int shiftY = y1 - current.y;
        Vec2 newBgPosition = _bg->getPosition();

        bool needToShiftBgX = !(x1 < origin.x + offsetX && shiftX > 0) || (x1 > visibleSize.width + origin.x - offsetX && shiftX < 0);

        if (needToShiftBgX && (x1 < origin.x + offsetX || x1 > visibleSize.width + origin.x - offsetX))
        {
            newBgPosition.x = _bg->getPosition().x - shiftX;
            if (newBgPosition.x < origin.x - (_bg->getContentSize().width - visibleSize.width))
            {
                newBgPosition.x = _bg->getPosition().x;
                x1 = std::min(x1, visibleSize.width + origin.x - offset);
            }
            else if (newBgPosition.x > origin.x)
            {
                newBgPosition.x = _bg->getPosition().x;
                x1 = std::max(x1, offset);
            }
            else
                x1 = current.x;
        }
        bool needToShiftBgY = !(y1 < origin.y + offsetY && shiftY > 0) || (y1 > visibleSize.height + origin.y - offsetY && shiftY < 0);
        if (needToShiftBgY && (y1 < origin.y + offsetY || y1 > visibleSize.height + origin.y - offsetY))
        {
            newBgPosition.y = _bg->getPosition().y - shiftY;
            if (newBgPosition.y < origin.y - (_bg->getContentSize().height - visibleSize.height))
            {
                newBgPosition.y = _bg->getPosition().y;
                y1 = std::min(y1, visibleSize.height + origin.y - offset);
            }
            else if (newBgPosition.y > origin.y)
            {
                newBgPosition.y = _bg->getPosition().y;
                y1 = std::max(offset, y1);
            }
            else
                y1 = current.y;
        }

        _bg->setPosition(newBgPosition);
    }
    else
    {
        int offset = this->getContentSize().width / 2;
        if (x1 < origin.x + offset || x1 > visibleSize.width + origin.x - offset)
            x1 = current.x;
        if (y1 < origin.y + offset || y1 > visibleSize.height + origin.y - offset)
            y1 = current.y;
    }


    this->setPosition(RTDataHelper::instance().convertToServerPoint(Vec2((int)x1, (int)y1), _bg->getPosition()));
    _dot->updatePositionFromPlayerPosition(this->getPosition(), _bg->getContentSize());
}

void PlayerObject::updatePosition(Vec2 newPos, float angle)
{
    _startMove = true;
    setPosition(newPos);
    float new_angle;
    if (abs(angle) > EPSILON)
    {
        new_angle = angle + 270;
        setPrevAngleDegree(angle);
    }
    else if (abs(getPrevAngleDegree()) > EPSILON)
        new_angle = getPrevAngleDegree() - 270;
    else
        new_angle = 0.0;
    setRotation(-new_angle);

    _dot->updatePositionFromPlayerPosition(this->getPosition(), _bg->getContentSize());
}

void PlayerObject::updatePositionWhenStatic(Vec2 globalPosition)
{
    if (_startMove)
        return;

    this->setPosition(globalPosition);
    _dot->updatePositionFromPlayerPosition(this->getPosition()/* - _bg->getPosition()*/, _bg->getContentSize());
}

void PlayerObject::updatePosition()
{
    if (_isAfterRespawn || !_startMove)
       return;

    float joystic_rotation = /*_targetAngle*/_prevAngleDegree;
    float new_angle;
    if (abs(joystic_rotation) > EPSILON)
    {
        float diff;

        if (abs(joystic_rotation - _prevAngleDegree) > 3.f)
        {
            diff = 3.f;
            float direction_of_move = 1; // 1 clockwise, -1 counterclock-wise

            if (joystic_rotation - _prevAngleDegree > 0 && joystic_rotation - _prevAngleDegree < 180)
               direction_of_move = 1;
            else if (joystic_rotation - _prevAngleDegree > 180)
               direction_of_move = -1;
            else if (joystic_rotation - _prevAngleDegree < 0 && joystic_rotation - _prevAngleDegree > -180)
               direction_of_move = -1;
            else
               direction_of_move = 1;

            diff *= direction_of_move;

            new_angle =  _prevAngleDegree + diff + 270;
            if (_prevAngleDegree + diff < 0)
              _prevAngleDegree = 360 + _prevAngleDegree + diff;
            else if (_prevAngleDegree + diff > 360)
              _prevAngleDegree = _prevAngleDegree + diff - 360;
            else
              _prevAngleDegree = _prevAngleDegree + diff;
        }
        else
        {
            new_angle =  joystic_rotation + 270;
            _prevAngleDegree = joystic_rotation;
        }
    }
    else if (abs(_prevAngleDegree) > EPSILON)
      new_angle = _prevAngleDegree + 270;
    else
      new_angle = 0.0;

    if (!(abs(joystic_rotation) > EPSILON || abs(helpers::degree_to_radians(_prevAngleDegree + 270)) > EPSILON))
      return;
    this->setRotation(-new_angle);

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    float x1, y1;

    x1 = this->getPosition().x + cos(helpers::degree_to_radians(_prevAngleDegree)) * _distanceCoeficient;
    y1 = this->getPosition().y + sin(helpers::degree_to_radians(_prevAngleDegree)) * _distanceCoeficient;
    auto current = RTDataHelper::instance().convertFromServerPoint(this->getPosition(), _bg->getPosition());
    auto future = RTDataHelper::instance().convertFromServerPoint(Vec2(x1, y1), _bg->getPosition());
    x1 = future.x;
    y1 = future.y;


    int offset = this->getContentSize().width / 2;
    if (x1 < origin.x + offset || x1 > visibleSize.width + origin.x - offset)
        x1 = current.x;
    if (y1 < origin.y + offset || y1 > visibleSize.height + origin.y - offset)
        y1 = current.y;


    this->setPosition(RTDataHelper::instance().convertToServerPoint(Vec2((int)x1, (int)y1), _bg->getPosition()));
    _dot->updatePositionFromPlayerPosition(this->getPosition(), _bg->getContentSize());
}

int PlayerObject::getHealth() const
{
    return _health;
}

float PlayerObject::getPrevAngleDegree() const
{
    return _prevAngleDegree;
}

PlayerData *PlayerObject::getPlayerData() const
{
    return _playerData;
}

PlayerTeamType PlayerObject::getPlayerTeamType() const
{
    return _teamType;
}

void PlayerObject::setPrevAngleDegree(float angle)
{
    _prevAngleDegree = angle;
}

void PlayerObject::setPlayerData(PlayerData *data)
{
    if (data)
        _playerData = data;
}

void PlayerObject::setPlayerTeamType(PlayerTeamType teamType)
{
    _teamType = teamType;
}

void PlayerObject::setSpawnGlobalPos(Vec2 globalPos)
{
    _spawnGlobalPos = globalPos;
}

void PlayerObject::bulletDamage(Bullet* bullet)
{
    if (_isAfterRespawn)
        return;

    if (bullet->getBulletType() == BulletType::dragon_breath)
    {
        if (!isCanBeDamagedByAreaObject(bullet->getPlayerData()->id))
            return;

        startDamageByAreaObject(bullet->getPlayerData()->id, 1.f);
        _health = std::max(0, _health - bullet->getBulletDamage());
    }
    else
    {
        _health = std::max(0, _health - bullet->getBulletDamage());
    }
}

void PlayerObject::mapItemDamage(std::string id, int damage)
{
    if (!isCanBeDamagedByAreaObject(id))
        return;
    startDamageByAreaObject(id, 0.5f);
    _health = std::max(0, _health - damage);
}

bool PlayerObject::isAfterRespawn() const
{
    return _isAfterRespawn;
}

bool PlayerObject::isCanBeDamagedByAreaObject(std::string id) const
{
    if (_isAfterRespawn)
        return false;

    for (int i = 0; i < _canBeDamagedByAreaObjects.size(); ++i)
    {
        if (_canBeDamagedByAreaObjects[i].first == id)
            return false;
    }

    return true;
}

bool PlayerObject::isCanBeDamagedByBullet(Bullet *bullet) const
{
    if (_isAfterRespawn)
        return false;

    if (bullet->getBulletType() != BulletType::dragon_breath)
        return true;
    else
        return isCanBeDamagedByAreaObject(bullet->getPlayerData()->id);
}

void PlayerObject::startDamageByAreaObject(std::string id, float tickLenght)
{
    if (!isCanBeDamagedByAreaObject(id))
        return;

    auto newObjData = std::make_pair(id, tickLenght);
    _canBeDamagedByAreaObjects.push_back(newObjData);
}

void PlayerObject::updateCanBeDamagedObjects(float dt)
{
    for (auto it = _canBeDamagedByAreaObjects.begin(); it != _canBeDamagedByAreaObjects.end(); )
    {
        (*it).second -= 0.1f;
        if ((*it).second < 0.f)
            it = _canBeDamagedByAreaObjects.erase(it);
        else
            ++it;
    }
}

Vec2 PlayerObject::getSpellPos(int type) const
{
    switch (type)
    {
    case BulletType::fireball :
        return getRotationChildPoint(_fireballPos);
    case BulletType::frostball :
        return getRotationChildPoint(_frostbalPos);
    case BulletType::dragon_breath :
        return getRotationChildPoint(_breathPos);
    }
}

void PlayerObject::setSpellsPos(Vec2 fire, Vec2 frost, Vec2 breath)
{
    _fireballPos = fire;
    _frostbalPos = frost;
    _breathPos = breath;
}

PlayerObject::PlayerObject(Vec2 spawnGlobalPos, PlayerTeamType teamType,
                           PlayerData *playerData, Sprite *bg, SmalMapDot* dot)
    : _spawnGlobalPos(spawnGlobalPos),
      _teamType(teamType),
      _playerData(playerData),
      _bg(bg),
      _dot(dot)
{
    _startBgPos = _bg->getPosition();
    _health = playerData->health;

    if (_teamType == PlayerTeamType::fire_team)
        _startRotationAngle = 90;
    else
        _startRotationAngle = 270;

    _prevAngleDegree = _startRotationAngle;
}

void PlayerObject::respawnFinished(float dt)
{
    if (_isCurrent)
        this->setOpacity(255);
    else
        this->setOpacity(200);
    _isAfterRespawn = false;
}

void PlayerObject::updateAfterRespawn()
{
    if (getOpacity() % 2 == 1)
        setOpacity(std::max(getOpacity() - 10, 0));
    else
        setOpacity(std::min(getOpacity() + 10, 255));
}

Vec2 PlayerObject::getRotationChildPoint(Vec2 childZeroAnglePos) const
{
    float angle = 2 * M_PI - helpers::degree_to_radians(getRotation());
    float x1 = childZeroAnglePos.x * cos(angle) - childZeroAnglePos.y * sin(angle);
    float y1 = childZeroAnglePos.y * cos(angle) + childZeroAnglePos.x * sin(angle);

    return Vec2(x1, y1);
}
