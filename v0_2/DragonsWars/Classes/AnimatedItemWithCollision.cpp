#include "AnimatedItemWithCollision.h"
#include <math.h>
#include "reader/CreatorReader.h"
#include "PhysicsShapeCache.h"
#include "Helpers.h"

AnimatedItemWithCollision *AnimatedItemWithCollision::createObject(std::string id, ItemWithCollisionType type)
{
    std::pair< std::string, int> animPathAndFramesCount;
    std::string bodyName;
    switch (type)
    {
    case ItemWithCollisionType::cloud1 :
        animPathAndFramesCount.first = "cloud1/cloud1%01d.png";
        animPathAndFramesCount.second = 1;
        bodyName = "cloud11";
        break;
    case ItemWithCollisionType::cloud2 :
        animPathAndFramesCount.first = "cloud2/cloud2%01d.png";
        animPathAndFramesCount.second = 1;
        bodyName = "cloud21";
        break;
    case ItemWithCollisionType::cloud3 :
        animPathAndFramesCount.first = "cloud3/cloud3%01d.png";
        animPathAndFramesCount.second = 1;
        bodyName = "cloud31";
        break;
    }
    auto frames = helpers::getAnimation(animPathAndFramesCount.first.c_str(), animPathAndFramesCount.second);
    auto item = new AnimatedItemWithCollision(id, frames.front());

    auto animation = Animation::createWithSpriteFrames(frames, 1.0f/8);
    item->runAction(RepeatForever::create(Animate::create(animation)));

    PhysicsBody* body = PhysicsShapeCache::getInstance()->createBodyWithName(bodyName);
    if (!body)
    {
        PhysicsShapeCache::getInstance()->addShapesWithFile("physics.plist");
        body = PhysicsShapeCache::getInstance()->createBodyWithName(bodyName);
        CC_ASSERT(body != nullptr);
    }

    item->setPhysicsBody(body);

    return item;
}

int AnimatedItemWithCollision::getDamage() const
{
    return 1;
}

std::string AnimatedItemWithCollision::getId() const
{
    return _id;
}

AnimatedItemWithCollision::AnimatedItemWithCollision(std::string id, SpriteFrame *frame)
    : _id(id)
{
    initWithSpriteFrame(frame);
}
