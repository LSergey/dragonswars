#include "PlayerProfileScene.h"
#include "platform/CCFileUtils.h"
#include "reader/CreatorReader.h"
#include "AppDelegate.h"
#include "ui/CocosGUI.h"
#include "Helpers.h"

Scene* PlayerProfileScene::createScene()
{
    PlayerProfileScene *pRet = new(std::nothrow) PlayerProfileScene();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

bool PlayerProfileScene::init() {

    if ( !Scene::init() ) {
        return false;
    }

    auto back_btn = utils::findChild<ui::Button*>(_currentScene, "closeBtn");
    back_btn->addClickEventListener([this](Ref*) {
        this->getParent()->setVisible(false);
    });

    setupData();

    return true;
}

void PlayerProfileScene::setupData()
{
    utils::findChild<Label*>(_currentScene, "levelNumber")->setString(
                helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->level));
    utils::findChild<Label*>(_currentScene, "winsNumber")->setString(
                helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->wins));
    utils::findChild<Label*>(_currentScene, "killsNumber")->setString(
                helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->kills));
    utils::findChild<Label*>(_currentScene, "totalDDNumber")->setString(
                helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->total_damage));
    utils::findChild<Label*>(_currentScene, "coinsEarnedNumber")->setString(
                helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->earned_coins));
    utils::findChild<Label*>(_currentScene, "losesNumber")->setString(
                helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->num_of_loses));
    utils::findChild<Label*>(_currentScene, "deathsNumber")->setString(
                helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->number_of_deaths));
    utils::findChild<Label*>(_currentScene, "damageRecievedNumber")->setString(
                helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->recieved_damage));
}

PlayerProfileScene::PlayerProfileScene()
{
    auto reader = creator::CreatorReader::createWithFilename("creator/PlayerProfileScene.ccreator");
    reader->setup();
    _currentScene = reader->getSceneGraph();
    addChild(_currentScene);
}
