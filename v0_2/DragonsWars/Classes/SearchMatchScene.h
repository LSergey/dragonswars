#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "BaseGameScene.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class SearchMatchScene : public Scene
{
public:
    static Scene* createScene(GameMode mode);
    
    virtual bool init();
    virtual void update(float dt);

    Scene *getScene() const;
private:
    SearchMatchScene(GameMode mode);
    ~SearchMatchScene();

    Scene* _currentScene = nullptr;
    LoadingBar* _loadingBar = nullptr;
    int _tmp_counter = 0;

    std::vector<Sprite*> joinedPlayersSprites;

    GameMode _gameMode;
};
