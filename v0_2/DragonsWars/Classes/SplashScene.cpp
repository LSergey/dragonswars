#include "SplashScene.h"
#include "MainScene.h"
#include "AppDelegate.h"
#include "GSMainManager.h"
#include "reader/CreatorReader.h"
#include "EnterNicknameScene.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include <android/log.h>
#endif

using namespace cocos2d;

cocos2d::Scene* SplashScene::createScene()
{
    SplashScene *pRet = new(std::nothrow) SplashScene();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

bool SplashScene::init()
{
    if ( !Scene::init() )
    {
        return false;
    }

    _loadingBar = static_cast<LoadingBar*>(utils::findChild(_currentScene, "progressBar"));
    
    this->scheduleUpdate();

    return true;
}

void SplashScene::update(float dt)
{
    //TODO refactor it when enought data be loaded
    _tmpCounter++;
    if (AppDelegate::sharedAppDelegate()->getGSManager().getStatus() == GSStatus::not_available)
    {
        for (int i = 1; i <= 5; ++i)
            _loadingBar->setPercent(i);
    }
    else if (AppDelegate::sharedAppDelegate()->getGSManager().getStatus() == GSStatus::authanticating)
    {
        for (int i = 6; i <= 20; ++i)
            _loadingBar->setPercent(i);
    }
    else if (AppDelegate::sharedAppDelegate()->getGSManager().getStatus() == GSStatus::authenticated_new && _loadingBar->getPercent() < 34.9)
    {
        AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->requestSetNewPlayerData();
        for (int i = 21; i <= 35; ++i)
            _loadingBar->setPercent(i);
    }
    else if (AppDelegate::sharedAppDelegate()->getGSManager().getStatus() == GSStatus::authenticated_old && _loadingBar->getPercent() < 34.9)
    {
        AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->requestPlayerData();
        for (int i = 21; i <= 35; ++i)
            _loadingBar->setPercent(i);
    }
    else if (AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->isDataLoaded() &&
             AppDelegate::sharedAppDelegate()->getGSManager().getStatus() == GSStatus::authenticated_new)
    {
        Director::getInstance()->replaceScene(EnterNicknameScene::createScene());
    }
    else if (AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->isDataLoaded() &&
             AppDelegate::sharedAppDelegate()->getGSManager().getStatus() == GSStatus::authenticated_old)
    {
        Director::getInstance()->replaceScene(MainScene::createScene());
    }
}

Scene *SplashScene::getScene() const
{
    return _currentScene;
}

SplashScene::SplashScene()
{
    auto reader = creator::CreatorReader::createWithFilename("creator/SplashScene.ccreator");
    reader->setup();
    _currentScene = reader->getSceneGraph();
    addChild(_currentScene);
}

SplashScene::~SplashScene()
{
}
