#pragma once

#include "cocos2d.h"

#include <GameSparksRT/IRTSessionListener.hpp>
#include <GameSparksRT/IRTSession.hpp>
#include <GameSparksRT/RTData.hpp>

using namespace GameSparks::RT;

class GameSession : IRTSessionListener
{
public:
    std::unique_ptr<IRTSession> Session;

    enum OpCode {SAY = 1, SHOUT, RESPOND};

    GameSession() = default;
    GameSession(const std::string& connectToken, const std::string& host, const std::string& port);

    void OnPlayerConnect(int peerId) override;
    void OnPlayerDisconnect(int peerId) override;
    void OnReady(bool ready) override;
    void OnPacket(const RTPacket& packet) override;
};
