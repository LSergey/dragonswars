#include "RTDataHelper.h"

using namespace cocos2d;

RTDataHelper &RTDataHelper::instance()
{
    static RTDataHelper helper;
    return helper;
}

Vec2 RTDataHelper::convertToServerPoint(const Vec2 &point, Vec2 bgPos)
{
    Vec2 result;
    result.x = point.x - bgPos.x;
    result.y = point.y - bgPos.y;

    return result;
}

Vec2 RTDataHelper::convertFromServerPoint(const Vec2 &point, Vec2 bgPos)
{
    Vec2 result;
    result.x = point.x + bgPos.x;
    result.y = point.y + bgPos.y;

    return result;
}
