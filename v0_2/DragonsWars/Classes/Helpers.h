#pragma once

#include <string>
#include <sstream>
#include <iomanip>
#include <math.h>
#include "cocos2d.h"

using namespace cocos2d;

namespace helpers {
    template <typename T>
    std::string to_string(T value)
    {
        std::ostringstream os ;
        os << value ;
        return os.str() ;
    }

    template <typename T>
    std::string to_string_with_precision(T value, int precision)
    {
        std::ostringstream os ;
        os << std::setprecision(precision) << std::fixed << value ;
        return os.str() ;
    }

    float degree_to_radians(float degree);
    float radioans_to_degree(float radians);
    Vector<SpriteFrame*> getAnimation(const char *format, int count);
}
