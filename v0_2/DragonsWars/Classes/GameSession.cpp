#include "GameSession.h"

#include <GameSparksRT/IRTSession.hpp>
#include <GameSparksRT/RTData.hpp>

#include "BaseGameScene.h"
#include "Helpers.h"

using namespace cocos2d;

GameSession::GameSession(const std::string &connectToken, const std::string &host, const std::string &port)
{
    Session.reset(GameSparks::RT::GameSparksRT
                  ::SessionBuilder()
                  .SetConnectToken(connectToken)
                  .SetHost(host)
                  .SetPort(port)
                  .SetListener(this)
                  .Build());

    Session->Start();
}

void GameSession::OnPlayerConnect(int peerId)
{
    cocos2d::log("RT::OnPlayerConnect: %d", peerId);
}

void GameSession::OnPlayerDisconnect(int peerId)
{
    cocos2d::log("RT::OnPlayerDisconnect: %d", peerId);
}

void GameSession::OnReady(bool ready)
{
    cocos2d::log("RT::OnReady: %s", ready?"true":"false");
}

void GameSession::OnPacket(const GameSparks::RT::RTPacket &packet)
{
    std::stringstream ss;
    ss << packet.Data;

    if (packet.OpCode == 50)
    {
        log("OnPacket. Opponent position: %s", ss.str().c_str());
        Scene* mm_scene = Director::getInstance()->getRunningScene();
        auto layer = dynamic_cast<BaseGameScene*>(mm_scene);
        if(layer != nullptr)
            layer->updateOpponentPosition(Vec2(packet.Data.GetInt(1).Value(), packet.Data.GetInt(2).Value()),
                                             packet.Data.GetInt(3).Value(), packet.Sender);
    }
    else if (packet.OpCode == 51)
    {
        log("OnPacket. Bullet fired: %s", ss.str().c_str());
        Scene* mm_scene = Director::getInstance()->getRunningScene();
        auto layer = dynamic_cast<BaseGameScene*>(mm_scene);
        if(layer != nullptr)
            layer->opponentFireBullet(packet.Sender, Vec2(packet.Data.GetInt(1).Value(), packet.Data.GetInt(2).Value()),
                                    packet.Data.GetInt(3).Value(), packet.Data.GetInt(4).Value(), packet.Data.GetInt(5).Value());
    }
    else if (packet.OpCode == 52)
    {
        log("OnPacket. Another player status changed: %s", ss.str().c_str());
        Scene* mm_scene = Director::getInstance()->getRunningScene();
        auto layer = dynamic_cast<BaseGameScene*>(mm_scene);
        if(layer != nullptr)
            layer->anotherPlayerStatusChanged(packet.Sender, packet.Data.GetInt(1).Value());
    }
//    else if (packet.OpCode == 101)
//    {
//        log("OnPacket. OpCode 101 %s", ss.str().c_str());
//    }
    else if (packet.OpCode == 70)
    {
        log("OnPacket. Spawn cloud: %s", ss.str().c_str());
        Scene* mm_scene = Director::getInstance()->getRunningScene();
        auto layer = dynamic_cast<BaseGameScene*>(mm_scene);
        if(layer != nullptr)
            layer->spawnCloud(packet.Data.GetInt(1).Value(), packet.Data.GetInt(2).Value(),
                              packet.Data.GetInt(3).Value(), packet.Data.GetInt(4).Value(),
                              helpers::to_string(packet.Data.GetInt(5).Value()));
    }
}
