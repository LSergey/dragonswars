﻿#include "BaseGameScene.h"
#include "Joystick.h"
#include "JoystickBase.h"
#include "AppDelegate.h"
#include "PlayerObject.h"
#include "RTDataHelper.h"
#include <string>
#include "Helpers.h"
#include "Bullet.h"
#include "reader/CreatorReader.h"
#include "AnimatedItemWithCollision.h"
#include "BattleResultScene.h"
#include "SmalMapDot.h"
#include "SpellSlot.h"
#include "ReceivedDamageSprite.h"
#include <cstdlib>

#include <functional>

USING_NS_CC;

static const float EPSILON = 0.00001 ;

Scene* BaseGameScene::createScene(GameMode mode)
{
    BaseGameScene *pRet = new(std::nothrow) BaseGameScene(mode);
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

bool BaseGameScene::init()
{
    if ( !Scene::init() )
        return false;

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tp.plist");

    scheduleUpdate();

    auto visibleSize = Director::getInstance()->getVisibleSize();
    //Vec2 origin = Director::getInstance()->getVisibleOrigin();
    float _shitToBorderValue = (visibleSize.height - _currentScene->getChildByName("creatorScene")->getChildByName("Canvas")->getContentSize().height ) / 2;

    auto node = utils::findChild<Node*>(_currentScene, "nodeWithZeroAnchor");
    node->setPosition(Vec2(node->getPosition().x, node->getPosition().y - _shitToBorderValue));
    _bg = utils::findChild<Sprite*>(_currentScene, "battle_bg");

    initPlayersPos();
    initStaticElements();

    this->schedule(schedule_selector(BaseGameScene::sendPlayerPos), 0.04f);

    //adds contact event listener
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(BaseGameScene::onContactBegin, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

//    auto collisionAnimation = AnimatedItemWithCollision::createObject(ItemWithCollisionType::cloud1);
//    _bg->addChild(collisionAnimation);
//    collisionAnimation->setPosition(1250,1250);
//    _mapAnimatedItesmWithCollision.push_back(collisionAnimation);

    _battleScore = std::make_pair<int, int>(0, 0);
    _teamsDD = std::make_pair<int, int>(0, 0);

    //float shiftToBorderValue = (visibleSize.width - _currentScene->getChildByName("creatorScene")->getChildByName("Canvas")->getContentSize().width ) / 2;
    auto topAlignNode = utils::findChild<Node*>(_currentScene, "topAlignNode");
    topAlignNode->setPosition(Vec2(topAlignNode->getPosition().x, topAlignNode->getPosition().y + _shitToBorderValue));
    auto bottomAlignNode = utils::findChild<Node*>(_currentScene, "bottomAlignNode");
    bottomAlignNode->setPosition(Vec2(bottomAlignNode->getPosition().x, bottomAlignNode->getPosition().y - _shitToBorderValue));

    updateScore();
    updatePlayerHealth();

    return true;
}

void BaseGameScene::update(float dt)
{
    if (_isPreparation || _battleEnded)
        return;

    updatePlayerPos();
    updateBullets(dt);
    updateStaticPlayersPos();
    updateRedSplash();
    updatePlayerHealth();
}

void BaseGameScene::additionalElementsUpdate()
{}

void BaseGameScene::initTopElementsLine() { }

void BaseGameScene::initPlayersPos()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    std::vector<Vec2> _other_player_positions_new;
    if (_gameMode == GameMode::oneXone)
    {
        _otherPlayerPositions.push_back(utils::findChild<Node*>(_currentScene, "1x1Player1Pos")->getPosition());
        _otherPlayerPositions.push_back(utils::findChild<Node*>(_currentScene, "1x1Player2Pos")->getPosition());
    }
    else
    {
        _otherPlayerPositions.push_back(utils::findChild<Node*>(_currentScene, "player1Pos")->getPosition());
        _otherPlayerPositions.push_back(utils::findChild<Node*>(_currentScene, "player2Pos")->getPosition());
        _otherPlayerPositions.push_back(utils::findChild<Node*>(_currentScene, "player3Pos")->getPosition());
        _otherPlayerPositions.push_back(utils::findChild<Node*>(_currentScene, "player4Pos")->getPosition());
    }

    auto fireTeam = AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getFireTeam();
    auto frostTeam = AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getFrostTeam();

    for (int i = 0; i < fireTeam.size(); ++i)
    {
        if (fireTeam[i]->id != AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->id)
        {
            auto playerDot = SmalMapDot::createObject(DotType::fire);
            utils::findChild<Node*>(_currentScene, "smallMapBg")->addChild(playerDot);

            auto other_player = PlayerObject::createObject(_otherPlayerPositions[i], PlayerTeamType::fire_team,
                                                           fireTeam[i], _bg, playerDot);
            other_player->setPosition(_otherPlayerPositions[i]);
            playerDot->updatePositionFromPlayerPosition(other_player->getPosition(), _bg->getContentSize());
            other_player->getPlayerData()->initBattleData((int)PlayerTeamType::fire_team);
            _otherPlayers.push_back(other_player);
            _other_player_positions_new.push_back(_otherPlayerPositions[i]);
            _bg->addChild(other_player, 100);
        }
        else
        {
            auto playerDot = SmalMapDot::createObject(DotType::fire);
            utils::findChild<Node*>(_currentScene, "smallMapBg")->addChild(playerDot);

            _currentPlayer = PlayerObject::createObject(_otherPlayerPositions[i], PlayerTeamType::fire_team,
                                                  fireTeam[i], _bg, playerDot);
            _currentPlayer->setPosition(_otherPlayerPositions[i]);
            playerDot->updatePositionFromPlayerPosition(_currentPlayer->getPosition(), _bg->getContentSize());
            _currentPlayer->getPlayerData()->initBattleData((int)PlayerTeamType::fire_team);
//            if (_gameMode == GameMode::oneXone)
//                _bg->setPosition(0.f, 0.f);
            _bg->addChild(_currentPlayer, 150);

        }
    }

    for (int i = 0; i < frostTeam.size(); ++i)
    {
        if (frostTeam[i]->id != AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->id)
        {
            auto playerDot = SmalMapDot::createObject(DotType::frost);
            utils::findChild<Node*>(_currentScene, "smallMapBg")->addChild(playerDot);

            auto other_player = PlayerObject::createObject(_otherPlayerPositions[i + _otherPlayerPositions.size() / 2],
                    PlayerTeamType::frost_team, frostTeam[i], _bg, playerDot);
            other_player->setPosition(_otherPlayerPositions[i + _otherPlayerPositions.size() / 2]);
            playerDot->updatePositionFromPlayerPosition(other_player->getPosition(), _bg->getContentSize());
            other_player->getPlayerData()->initBattleData((int)PlayerTeamType::frost_team);
            _otherPlayers.push_back(other_player);
            _other_player_positions_new.push_back(_otherPlayerPositions[i + _otherPlayerPositions.size() / 2]);
            _bg->addChild(other_player, 100);
        }
        else
        {
            auto playerDot = SmalMapDot::createObject(DotType::frost);
            utils::findChild<Node*>(_currentScene, "smallMapBg")->addChild(playerDot);

            _currentPlayer = PlayerObject::createObject(_otherPlayerPositions[i + _otherPlayerPositions.size() / 2],
                    PlayerTeamType::frost_team, frostTeam[i], _bg, playerDot);
            _currentPlayer->setPosition(_otherPlayerPositions[i + _otherPlayerPositions.size() / 2]);
            playerDot->updatePositionFromPlayerPosition(_currentPlayer->getPosition(), _bg->getContentSize());
            _currentPlayer->getPlayerData()->initBattleData((int)PlayerTeamType::frost_team);
            _bg->addChild(_currentPlayer, 150);

//            if (_gameMode == GameMode::oneXone)
//            {
//                float shiftToBorderValue = (visibleSize.width - _currentScene->getChildByName("creatorScene")->getChildByName("Canvas")->getContentSize().width ) / 2;
//                _bg->setPosition(visibleSize.width - _bg->getContentSize().width + shiftToBorderValue, 0.f);
//            }
        }
    }
    _otherPlayerPositions = _other_player_positions_new;
    _bgStartPos = _bg->getPosition();
    updateStaticPlayersPos();
}

void BaseGameScene::initStaticElements()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    // add joystick
    auto joystick_pad_prefab = dynamic_cast<Sprite*>(utils::findChild(_currentScene, "joystickPad"));
    Vec2 pos = joystick_pad_prefab->getPosition();
    joystick_pad_prefab->removeFromParent();
    auto joystick_prefab = dynamic_cast<Sprite*>(utils::findChild(_currentScene, "joystick"));
    joystick_prefab->removeFromParent();

    joystick_pad_prefab->setPosition(Vec2(0, 0));
    joystick_prefab->setPosition(Vec2(0, 0));

    _joystickBase = SneakyJoystickSkinnedBase::create();
    auto joystick = new SneakyJoystick();
    joystick->initWithRect(Rect(Vec2(0, 0), joystick_pad_prefab->getContentSize()));
    _joystickBase->setBackgroundSprite(joystick_pad_prefab);
    _joystickBase->setThumbSprite(joystick_prefab);
    _joystickBase->setJoystick(joystick);
    _joystickBase->setPosition(pos);
    utils::findChild(_currentScene, "bottomAlignNode")->addChild(_joystickBase);

    _playerHealthBar = static_cast<LoadingBar*>(utils::findChild(_currentScene, "playerHealth"));
    _playerHealthBar->setPercent(100);

    auto preparationLabel = utils::findChild<Label*>(_currentScene, "timeToBattleStart");
    preparationLabel->setString(helpers::to_string(_preparationTime));
    this->schedule(schedule_selector(BaseGameScene::updatePreparationLabel), 1.f);

    auto timeLabel = utils::findChild<Label*>(_currentScene, "timeLabel");
    timeLabel->setString(helpers::to_string(_secondsLeft));

    auto fireTeam = AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getFireTeam();
    auto frostTeam = AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getFrostTeam();
    PlayerData* currentPlayerData;
    for (int i = 0; i < fireTeam.size(); ++i)
    {
        if (fireTeam[i]->id == AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->id)
            currentPlayerData = fireTeam[i];
    }
    for (int i = 0; i < frostTeam.size(); ++i)
    {
        if (frostTeam[i]->id == AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->id)
            currentPlayerData = frostTeam[i];
    }

    spellSlot1 = SpellSlot::createObject(BulletType::dragon_breath, currentPlayerData, _currentPlayer->getPlayerTeamType());
    spellSlot2 = SpellSlot::createObject(BulletType::frostball, currentPlayerData, _currentPlayer->getPlayerTeamType());
    spellSlot3 = SpellSlot::createObject(BulletType::fireball, currentPlayerData, _currentPlayer->getPlayerTeamType());
    auto centeredNode = utils::findChild(_currentScene, "bottomAlignNode");
    spellSlot1->setPosition(utils::findChild(_currentScene, "slot1")->getPosition());
    spellSlot2->setPosition(utils::findChild(_currentScene, "slot2")->getPosition());
    spellSlot3->setPosition(utils::findChild(_currentScene, "slot3")->getPosition());
    utils::findChild(_currentScene, "slot1")->setVisible(false);
    utils::findChild(_currentScene, "slot2")->setVisible(false);
    utils::findChild(_currentScene, "slot3")->setVisible(false);
    centeredNode->addChild(spellSlot1);
    centeredNode->addChild(spellSlot2);
    centeredNode->addChild(spellSlot3);

    spellSlot3->addTouchEventListener(CC_CALLBACK_2(BaseGameScene::throwFireball, this));
    spellSlot2->addTouchEventListener(CC_CALLBACK_2(BaseGameScene::throwFrostball, this));
    spellSlot1->addTouchEventListener(CC_CALLBACK_2(BaseGameScene::releaseDragonthBreath, this));

    auto spritecache = SpriteFrameCache::getInstance();
    _redSplash = Sprite::createWithSpriteFrame(spritecache->getSpriteFrameByName("redScreenSplash/redSplash.png"));
    _redSplash->setAnchorPoint(Vec2(0, 0));
    _redSplash->setPosition(Vec2(0, 0));
    _redSplash->setScaleX(visibleSize.width / _redSplash->getContentSize().width);
    _redSplash->setOpacity(100);
    _redSplash->setVisible(false);
    _currentScene->addChild(_redSplash);
}

void BaseGameScene::updatePlayerPos()
{
    _currentPlayer->updatePositionWithBgShift(_joystickBase->getJoystick()->getDegrees());
    //log("Update current position: (%f, %f) deg: %f", _currentPlayer->getPosition().x, _currentPlayer->getPosition().y, _currentPlayer->getRotation());
}

void BaseGameScene::updateOpponentPosition(Vec2 pos, float degree, int peerId)
{
    //log("Update opponent position: (%f, %f) deg: %f", pos.x, pos.y, degree);
    for (int i = 0; i < _otherPlayers.size(); ++i)
    {
        if (_otherPlayers[i]->getPlayerData()->peerId == peerId)
        {
            _otherPlayers[i]->updatePosition(pos, degree);
            break;
        }
    }
}

void BaseGameScene::opponentFireBullet(int peerId, Vec2 pos, int degree, int bulletType, int bulletLevel)
{
    for (int i = 0; i < _otherPlayers.size(); ++i)
    {
        if (_otherPlayers[i]->getPlayerData()->peerId == peerId)
        {
            auto bullet = Bullet::createObject((BulletType)bulletType, bulletLevel, _otherPlayers[i], _bg);
            bullet->setPosition(pos);
            bullet->setRotation(degree);
            bullet->setPlayerTeamType(_otherPlayers[i]->getPlayerTeamType());

            if (_otherPlayers[i]->getPlayerTeamType() == _currentPlayer->getPlayerTeamType())
                _myBullets.push_back(bullet);
            else
                _opponentsBullets.push_back(bullet);

            _bg->addChild(bullet);
            break;
        }
    }
}

void BaseGameScene::anotherPlayerStatusChanged(int peerId, int isDie)
{
    for (int i = 0; i < _otherPlayers.size(); ++i)
    {
        if (_otherPlayers[i]->getPlayerData()->peerId == peerId)
        {
            if (isDie)
            {
                if (!_otherPlayers[i]->isAfterRespawn())
                    _otherPlayers[i]->respawn(false);
            }
            break;
        }
    }
}

void BaseGameScene::requestBattleDataResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response)
{
    if (response.GetHasErrors())
    {
    }
    else
    {

    }
}

void BaseGameScene::sendPlayerPos(float)
{
    if (_isPreparation || _battleEnded)
        return;

    AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->
            sendPlayerPos(_currentPlayer->getPosition(),
                          _currentPlayer->getPrevAngleDegree());
}

void BaseGameScene::updateMatchTimer(float)
{
    if (_isPreparation || _battleEnded)
        return;

    utils::findChild<Label*>(_currentScene, "timeLabel")->setString(helpers::to_string(--_secondsLeft));
    if (_secondsLeft == 0)
    {
        _battleEnded = true;
        if (this->_scenesLayer)
        {
            _scenesLayer->removeFromParent();
        }
        _scenesLayer = Layer::create();
        _scenesLayer->addChild(BattleResultScene::createScene(_battleScore, _teamsDD));
        this->_currentScene->addChild(_scenesLayer);
    }
}

void BaseGameScene::updateBullets(float)
{
    for (auto it = _myBullets.begin(); it != _myBullets.end(); )
    {
        Bullet* bullet = *it;
        if (bullet->isLifetimeEnded())
        {
            it = _myBullets.erase(it);
            _bg->removeChild(bullet, false);
            if (it == _myBullets.end())
                break;
        }
        else
            ++it;
    }

    for (auto it = _opponentsBullets.begin(); it != _opponentsBullets.end(); )
    {
        Bullet* bullet = *it;
        if (bullet->isLifetimeEnded())
        {
            it = _opponentsBullets.erase(it);
            _bg->removeChild(bullet, false);
            if (it == _opponentsBullets.end())
                break;
        }
        else
            ++it;
    }

    for (int i = 0; i < _myBullets.size(); i++)
        _myBullets[i]->updatePosition();

    for (int i = 0; i < _opponentsBullets.size(); i++)
        _opponentsBullets[i]->updatePosition();
}

void BaseGameScene::updateStaticPlayersPos()
{
    for (int i = 0; i < _otherPlayers.size(); ++i)
        _otherPlayers[i]->updatePositionWhenStatic(_otherPlayerPositions[i]);
}

void BaseGameScene::updatePreparationLabel(float)
{
    if (_preparationTime == 1)
    {
        _isPreparation = false;
        utils::findChild<Label*>(_currentScene, "timeToBattleStart")->setVisible(false);
        this->schedule(schedule_selector(BaseGameScene::updateMatchTimer), 1.f);
        this->unschedule(schedule_selector(BaseGameScene::updatePreparationLabel));
    }
    else
        utils::findChild<Label*>(_currentScene, "timeToBattleStart")->setString(helpers::to_string(--_preparationTime));
}

void BaseGameScene::updateScore()
{
    utils::findChild<Label*>(_currentScene, "fireTeamScore")->setString(helpers::to_string(_battleScore.first));
    utils::findChild<Label*>(_currentScene, "frostTeamScore")->setString(helpers::to_string(_battleScore.second));
}

void BaseGameScene::updateRedSplash()
{
    if (!_redSplash->isVisible())
        return;

    if (_redSplash->getOpacity() == 1)
    {
        _redSplash->setVisible(false);
        return;
    }

    _redSplash->setOpacity(_redSplash->getOpacity() - 1);
}

void BaseGameScene::updatePlayerHealth()
{
    auto coins_count = utils::findChild<Label*>(_currentScene, "playerHealthLabel");
    coins_count->setString(helpers::to_string(_currentPlayer->getHealth()));
}

Scene *BaseGameScene::getScene() const
{
    return _currentScene;
}

void BaseGameScene::throwFireball(Ref* pSender, ui::Widget::TouchEventType type)
{
    if (_isPreparation || _battleEnded || _currentPlayer->isAfterRespawn())
        return;

    if (type == ui::Widget::TouchEventType::ENDED)
    {
        if (spellSlot3->isCooldown())
            return;
        spellSlot3->activateCooldown();
        auto bullet = Bullet::createObject(BulletType::fireball,
                                           _currentPlayer->getPlayerData()->fireball_level, _currentPlayer, _bg);
        Vec2 bulletPos = _currentPlayer->getPosition() + _currentPlayer->getSpellPos((int)BulletType::fireball);
        bullet->setPosition(Vec2((int)bulletPos.x, (int)bulletPos.y));
        bullet->setRotation((int)_currentPlayer->getRotation());
        bullet->setPlayerTeamType(_currentPlayer->getPlayerTeamType());

        _myBullets.push_back(bullet);
        _bg->addChild(bullet);

        AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->
                fireBullet(bullet->getPosition(), (int)bullet->getRotation(), (int)bullet->getBulletType(), bullet->getBulletLevel());
    }
}

void BaseGameScene::throwFrostball(Ref *pSender, Widget::TouchEventType type)
{
    if (_isPreparation || _battleEnded || _currentPlayer->isAfterRespawn())
        return;

    if (type == ui::Widget::TouchEventType::ENDED)
    {
        if (spellSlot2->isCooldown())
            return;
        spellSlot2->activateCooldown();
        auto bullet = Bullet::createObject(BulletType::frostball,
                                           _currentPlayer->getPlayerData()->frostball_level,
                                           _currentPlayer, _bg);
        Vec2 bulletPos = _currentPlayer->getPosition() + _currentPlayer->getSpellPos((int)BulletType::frostball);
        bullet->setPosition(Vec2((int)bulletPos.x, (int)bulletPos.y));
        bullet->setRotation((int)_currentPlayer->getRotation());
        bullet->setPlayerTeamType(_currentPlayer->getPlayerTeamType());

        _myBullets.push_back(bullet);
        _bg->addChild(bullet);

        AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->
                fireBullet(bullet->getPosition(), (int)bullet->getRotation(), (int)bullet->getBulletType(), bullet->getBulletLevel());
    }
}

void BaseGameScene::releaseDragonthBreath(Ref *pSender, Widget::TouchEventType type)
{
    if (_isPreparation || _battleEnded || _currentPlayer->isAfterRespawn())
        return;

    if (type == ui::Widget::TouchEventType::ENDED)
    {
        if (spellSlot1->isCooldown())
            return;
        spellSlot1->activateCooldown();
        auto bullet = Bullet::createObject(BulletType::dragon_breath,
                                           _currentPlayer->getPlayerData()->dragon_breath_level, _currentPlayer, _bg);
        bullet->setRotation(_currentPlayer->getRotation());
        bullet->setPlayerTeamType(_currentPlayer->getPlayerTeamType());

        _myBullets.push_back(bullet);
        _bg->addChild(bullet);

        AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->
                fireBullet(bullet->getPosition(), (int)bullet->getRotation(), (int)bullet->getBulletType(), bullet->getBulletLevel());
    }
}

Vector<SpriteFrame*> BaseGameScene::getAnimation(const char *format, int count)
{
    auto spritecache = SpriteFrameCache::getInstance();
    Vector<SpriteFrame*> animFrames;
    char str[100];
    for(int i = 1; i <= count; i++)
    {
        sprintf(str, format, i);
        animFrames.pushBack(spritecache->getSpriteFrameByName(str));
    }
    return animFrames;
}

void BaseGameScene::requestBattleDataChange(std::string type, int amount)
{
    GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
    request.SetEventKey("addBattleStats");
    request.SetEventAttribute("type", type);
    request.SetEventAttribute("amount", amount);

    std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
            std::bind(&BaseGameScene::requestBattleDataResponse, this, std::placeholders::_1, std::placeholders::_2);
    request.Send(callback, 0);
}

void BaseGameScene::activateRedSplash()
{
    _redSplash->setVisible(true);
    _redSplash->setOpacity(100);
}

void BaseGameScene::spawnCloud(int type, int yPos, int xSide, int moveToTime, std::string id)
{
    auto collisionAnimation = AnimatedItemWithCollision::createObject(id, (ItemWithCollisionType)type);

    int xPos = xSide ? -collisionAnimation->getContentSize().width : _bg->getContentSize().width + collisionAnimation->getContentSize().width;
    collisionAnimation->setPosition(xPos, yPos);
    Vec2 moveToPos = xPos > 0 ? Vec2(-collisionAnimation->getContentSize().width, yPos) :
                                Vec2(_bg->getContentSize().width + collisionAnimation->getContentSize().width, yPos);
    auto moveTo = MoveTo::create(moveToTime, moveToPos);
    collisionAnimation->runAction(moveTo);

    _bg->addChild(collisionAnimation, 200);
    _mapAnimatedItesmWithCollision.push_back(collisionAnimation);
}

BaseGameScene::BaseGameScene(GameMode mode)
    : _gameMode(mode)
{
    initWithPhysics();
    auto reader = creator::CreatorReader::createWithFilename("creator/BattleScene.ccreator");
    reader->setup();

    _currentScene = reader->getSceneGraph(true);
    //getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    addChild(_currentScene);
}

BaseGameScene::~BaseGameScene()
{
}

bool BaseGameScene::onContactBegin(PhysicsContact &contact)
{
    auto nodeA = contact.getShapeA()->getBody()->getNode();
    auto nodeB = contact.getShapeB()->getBody()->getNode();
    if (nodeA && nodeA->getTag() == 1)
        nodeA = nodeA->getParent();
    if (nodeB && nodeB->getTag() == 1)
        nodeB = nodeA->getParent();

    auto nodeAPlayer = dynamic_cast<PlayerObject*>(nodeA);
    auto nodeBPlayer = dynamic_cast<PlayerObject*>(nodeB);
    auto nodeABullet = dynamic_cast<Bullet*>(nodeA);
    auto nodeBBullet = dynamic_cast<Bullet*>(nodeB);
    auto nodeAMapItem = dynamic_cast<AnimatedItemWithCollision*>(nodeA);
    auto nodeBMapItem = dynamic_cast<AnimatedItemWithCollision*>(nodeB);

    PlayerObject* collisionPlayer = nullptr;
    Bullet* collisionBullet = nullptr;
    AnimatedItemWithCollision* mapItem = nullptr;

    if ((!nodeAPlayer && !nodeABullet && !nodeAMapItem) || (!nodeBPlayer && !nodeBBullet && !nodeBMapItem))
        return false;

    if ((nodeAPlayer && nodeBPlayer) || (nodeABullet && nodeBBullet))
        return false;

    if ((nodeAMapItem && nodeBMapItem) || (nodeAMapItem && nodeBBullet) || (nodeBMapItem && nodeABullet))
        return false;

    if (nodeAPlayer && nodeBBullet)
    {
        if (nodeAPlayer->getPlayerTeamType() == nodeBBullet->getPlayerTeamType())
            return false;

        collisionPlayer = nodeAPlayer;
        collisionBullet = nodeBBullet;
    }

    if (nodeBPlayer && nodeABullet)
    {
        if (nodeBPlayer->getPlayerTeamType() == nodeABullet->getPlayerTeamType())
            return false;

        collisionPlayer = nodeBPlayer;
        collisionBullet = nodeABullet;
    }

    if (nodeAMapItem || nodeBMapItem)
    {
        if (nodeAMapItem)
        {
            collisionPlayer = nodeBPlayer;
            mapItem = nodeAMapItem;
        }
        else
        {
            collisionPlayer = nodeAPlayer;
            mapItem = nodeBMapItem;
        }
        if (collisionPlayer == _currentPlayer && collisionPlayer->isCanBeDamagedByAreaObject(mapItem->getId()))
        {
            activateRedSplash();
            requestBattleDataChange("recieved_d", std::min(mapItem->getDamage(), collisionPlayer->getHealth()));

            auto damageSprite = ReceivedDamageSprite::createObject(mapItem->getDamage(),
                Vec2(_playerHealthBar->getContentSize().width / 2, _playerHealthBar->getContentSize().height / 2));

            _playerHealthBar->addChild(damageSprite);
        }
        collisionPlayer->mapItemDamage(mapItem->getId(), mapItem->getDamage());
        if (collisionPlayer->getHealth() == 0)
        {
            collisionPlayer->respawn(collisionPlayer == _currentPlayer);
            if (collisionPlayer == _currentPlayer)
            {
                requestBattleDataChange("deaths", 1);
                AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->
                        sendPlayerStatus(true);
            }
        }
        if (collisionPlayer == _currentPlayer)
            _playerHealthBar->setPercent(100 * _currentPlayer->getHealth() / _currentPlayer->getPlayerData()->health);
        return true;
    }

    if (collisionPlayer->getPlayerTeamType() == _currentPlayer->getPlayerTeamType())
    {
        for (auto it = _opponentsBullets.begin(); it != _opponentsBullets.end(); ++it)
        {
            if (collisionBullet == *it)
            {
                int damage = collisionPlayer->getHealth() > collisionBullet->getBulletDamage() ?
                            collisionBullet->getBulletDamage() : collisionPlayer->getHealth();
                if (collisionPlayer->isCanBeDamagedByBullet(collisionBullet))
                {
                    collisionBullet->getPlayerData()->getBattleData()->dealDamage += damage;
                    if (collisionPlayer->getPlayerTeamType() == PlayerTeamType::fire_team)
                        _teamsDD.second += damage;
                    else
                        _teamsDD.first += damage;
                }

                if (collisionPlayer == _currentPlayer && collisionPlayer->isCanBeDamagedByBullet(collisionBullet))
                {
                    activateRedSplash();
                    requestBattleDataChange("recieved_d", collisionBullet->getBulletDamage());

                    auto damageSprite = ReceivedDamageSprite::createObject(collisionBullet->getBulletDamage(),
                        Vec2(_playerHealthBar->getContentSize().width / 2, _playerHealthBar->getContentSize().height / 2));

                    _playerHealthBar->addChild(damageSprite);
                }

                collisionPlayer->bulletDamage(collisionBullet);
                if (collisionPlayer->getHealth() == 0)
                {
                    if (collisionPlayer->getPlayerTeamType() == PlayerTeamType::fire_team)
                    {
                        _battleScore.second += 1;
                        updateScore();
                    }
                    else
                    {
                        _battleScore.first += 1;
                        updateScore();
                    }
                    collisionBullet->getPlayerData()->getBattleData()->kills += 1;
                    collisionPlayer->respawn(collisionPlayer == _currentPlayer);
                    if (collisionPlayer == _currentPlayer)
                        _bg->setPosition(_bgStartPos);
                    if (collisionPlayer == _currentPlayer)
                    {
                        requestBattleDataChange("deaths", 1);
                        AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->
                                sendPlayerStatus(true);
                    }
                }
                if (collisionPlayer == _currentPlayer)
                    _playerHealthBar->setPercent(100 * _currentPlayer->getHealth() / _currentPlayer->getPlayerData()->health);

                Bullet* bullet = *it;
                if (bullet->getBulletType() != BulletType::dragon_breath)
                {
                    _opponentsBullets.erase(it);
                    _bg->removeChild(bullet, false);
                }
                break;
            }
        }
    }
    else
    {
        for (auto it = _myBullets.begin(); it != _myBullets.end(); ++it)
        {
            if (collisionBullet == *it)
            {
                int damage = collisionPlayer->getHealth() > collisionBullet->getBulletDamage() ?
                            collisionBullet->getBulletDamage() : collisionPlayer->getHealth();
                if (collisionPlayer->isCanBeDamagedByBullet(collisionBullet))
                {
                    collisionBullet->getPlayerData()->getBattleData()->dealDamage += damage;
                    if (collisionPlayer->getPlayerTeamType() == PlayerTeamType::fire_team)
                        _teamsDD.second += damage;
                    else
                        _teamsDD.first += damage;
                }

                collisionPlayer->bulletDamage(collisionBullet);
                if (collisionBullet->getPlayerData()->id == _currentPlayer->getPlayerData()->id && !collisionPlayer->isAfterRespawn())
                    requestBattleDataChange("total_dd", collisionBullet->getBulletDamage());

                if (collisionPlayer->getHealth() == 0)
                {
                    if (collisionPlayer->getPlayerTeamType() == PlayerTeamType::fire_team)
                    {
                        _battleScore.second += 1;
                        updateScore();
                    }
                    else
                    {
                        _battleScore.first += 1;
                        updateScore();
                    }
                    collisionBullet->getPlayerData()->getBattleData()->kills += 1;
                    if (collisionBullet->getPlayerData()->id == _currentPlayer->getPlayerData()->id)
                        requestBattleDataChange("kills", 1);
                    collisionPlayer->respawn(collisionPlayer == _currentPlayer);
                }
                Bullet* bullet = *it;
                if (bullet->getBulletType() != BulletType::dragon_breath)
                {
                    _myBullets.erase(it);
                    _bg->removeChild(bullet, false);
                }
                break;
            }
        }
    }

    return true;
}
