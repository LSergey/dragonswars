#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

#include <GameSparks/generated/GSRequests.h>
#include <GameSparks/generated/GSMessages.h>

using namespace GameSparks::Api::Messages;

using namespace cocos2d;
using namespace cocos2d::ui;

class SkinsScene : public cocos2d::Scene
{
public:

    static Scene* createScene();
    
    virtual bool init();
    virtual void update(float dt);

    Scene *getScene() const;
    
protected:
    SkinsScene();
    ~SkinsScene();

    Scene* _currentScene = nullptr;
};


class SkinSceneItem: public Sprite
{
public:
    static SkinSceneItem* createObject(int dragonId);
    int getId() const;
    bool isNeedBuy() const;

    void requestUseResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response);
    void requestBuyResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response);
    void buttonPresed(Ref* pSender, ui::Widget::TouchEventType type);

private:
    SkinSceneItem(int dragonId);
    ~SkinSceneItem() = default;

    bool _needBuy = true;
    int _dragonId;

};
