#pragma once

#include <memory>

#include "GameSession.h"
#include "GSAuthManager.h"
#include "GSMatchManager.h"
#include <GameSparks/GS.h>
#include <GameSparks/Cocos2dxPlatform.h>
#include "PlayerDataManager.h"

using namespace GameSparks::Core;

enum class GSStatus {not_available = 0, authanticating, authenticated_new, authenticated_old};

class GSMainManager
{
public:
    GSMainManager();
    ~GSMainManager() = default;
    void init();

    void onGameSparksAvailable(GameSparks::Core::GS& gsInstance, bool available);

    void update(float dt);

    GSStatus getStatus() const;
    std::unique_ptr<GSMatchManager>& getMatchManager();
    std::unique_ptr<PlayerDataManager> &getPlayerDataManager();
    std::shared_ptr<GameSparks::Core::GS> m_gs;
    std::shared_ptr<GameSession> m_rtSession;

private:
    GSStatus m_status = GSStatus::not_available;

    std::unique_ptr<GSAuthManager> _authManager;
    std::unique_ptr<GSMatchManager> _matchManager;
    std::unique_ptr<PlayerDataManager> _playerDataManager;

    Cocos2dxPlatform _platform;
};
