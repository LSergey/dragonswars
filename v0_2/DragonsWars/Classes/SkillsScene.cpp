#include "SkillsScene.h"
#include "platform/CCFileUtils.h"
#include "reader/CreatorReader.h"
#include "AppDelegate.h"
#include "ui/CocosGUI.h"
#include "Helpers.h"
#include "Bullet.h"
#include "ParamsManager.h"

Scene* SkillsScene::createScene()
{
    SkillsScene *pRet = new(std::nothrow) SkillsScene();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

bool SkillsScene::init() {

    if ( !Scene::init() ) {
        return false;
    }

    auto back_btn = utils::findChild<ui::Button*>(_currentScene, "backBtn");
    back_btn->addClickEventListener([](Ref*) {
        Director::getInstance()->popScene();
    });

    setupFireballUpgrade();
    setupFrostballUpgrade();
    setupDragonthBreathUpgrade();

    scheduleUpdate();

    return true;
}

void SkillsScene::requestUpgradeResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response)
{
    if (response.GetHasErrors())
    {
    }
    else
    {
        AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->requestPlayerData();
    }
}

void SkillsScene::requestUpgradeExpResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response)
{
    if (response.GetHasErrors())
    {
    }
    else
    {
        AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->requestPlayerData();
    }
}

SkillsScene::SkillsScene()
{
    auto reader = creator::CreatorReader::createWithFilename("creator/SkillsScene.ccreator");
    reader->setup();
    _currentScene = reader->getSceneGraph();
    addChild(_currentScene);
}

void SkillsScene::update(float dt)
{
    setupFireballUpgrade();
    setupFrostballUpgrade();
    setupDragonthBreathUpgrade();
}

void SkillsScene::setupFireballUpgrade()
{
    auto upgradeNode = utils::findChild(_currentScene, "fireballBorder");
    int currentLevel = AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->fireball_level;
    int nextLevel = currentLevel + 1;
    int currentDamage, nextDamage;
    float currentCooldawn, nextCooldawn;
    float currentAdditional, nextAdditional;
    float lifetime, nextLifetime;

    std::tie(currentDamage, currentCooldawn, currentAdditional, lifetime) =
            ParamsManager::instance()->getSpellParams(
                AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(),
                BulletType::fireball, currentLevel);

    std::tie(nextDamage, nextCooldawn, nextAdditional, nextLifetime) =
            ParamsManager::instance()->getSpellParams(
                AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(),
                BulletType::fireball, nextLevel);

    Label* levelLabel = utils::findChild<Label*>(upgradeNode, "levelLabel");
    Label* currentDamageLabel = utils::findChild<Label*>(upgradeNode, "currentDamageLabel");
    Label* nextDamageLabel = utils::findChild<Label*>(upgradeNode, "nextDamageLabel");
    Label* currentCooldawnLabel = utils::findChild<Label*>(upgradeNode, "currentCooldawnLabel");
    Label* nextCooldawnLabel = utils::findChild<Label*>(upgradeNode, "nextCooldawnLabel");
    Label* currentAdditionalLabel = utils::findChild<Label*>(upgradeNode, "currentAdditionalLabel");
    Label* nextAdditionalLabel = utils::findChild<Label*>(upgradeNode, "nextAdditionalLabel");

    levelLabel->setString("Level " + helpers::to_string(currentLevel));
    currentDamageLabel->setString(helpers::to_string(currentDamage));
    nextDamageLabel->setString("+" + helpers::to_string(nextDamage - currentDamage));
    currentCooldawnLabel->setString(helpers::to_string(currentCooldawn));
    nextCooldawnLabel->setString("-" + helpers::to_string(currentCooldawn - nextCooldawn));
    currentAdditionalLabel->setString(helpers::to_string(lifetime));
    nextAdditionalLabel->setString("+" + helpers::to_string(lifetime - nextLifetime));

    int cost = std::get<0>(ParamsManager::instance()->getSpellUpgradeParams(
                   AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(), BulletType::fireball));
    auto button = utils::findChild<Button*>(upgradeNode, "upgradeBtn");
    auto fontSize = button->getTitleLabel()->getSystemFontSize();
    auto color = button->getTitleLabel()->getTextColor();
    button->setTitleText("Upgrade \n " + helpers::to_string(cost));
    button->getTitleLabel()->setSystemFontSize(fontSize);
    button->getTitleLabel()->setTextColor(color);
    if (AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->dragon_coins < cost)
        button->setEnabled(false);
    else
        button->setEnabled(true);
    button->addTouchEventListener(CC_CALLBACK_2(SkillsScene::upgradeFireballPressed, this));
}

void SkillsScene::setupFrostballUpgrade()
{
    auto upgradeNode = utils::findChild(_currentScene, "frostballBorder");
    int currentLevel = AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->frostball_level;
    int nextLevel = currentLevel + 1;
    int currentDamage, nextDamage;
    float currentCooldawn, nextCooldawn;
    float currentAdditional, nextAdditional;
    float lifetime, nextLifetime;

    std::tie(currentDamage, currentCooldawn, currentAdditional, lifetime) =
            ParamsManager::instance()->getSpellParams(
                AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(),
                BulletType::frostball, currentLevel);

    std::tie(nextDamage, nextCooldawn, nextAdditional, nextLifetime) =
            ParamsManager::instance()->getSpellParams(
                AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(),
                BulletType::frostball, nextLevel);

    Label* levelLabel = utils::findChild<Label*>(upgradeNode, "levelLabel");
    Label* currentDamageLabel = utils::findChild<Label*>(upgradeNode, "currentDamageLabel");
    Label* nextDamageLabel = utils::findChild<Label*>(upgradeNode, "nextDamageLabel");
    Label* currentCooldawnLabel = utils::findChild<Label*>(upgradeNode, "currentCooldawnLabel");
    Label* nextCooldawnLabel = utils::findChild<Label*>(upgradeNode, "nextCooldawnLabel");
    Label* currentAdditionalLabel = utils::findChild<Label*>(upgradeNode, "currentAdditionalLabel");
    Label* nextAdditionalLabel = utils::findChild<Label*>(upgradeNode, "nextAdditionalLabel");

    levelLabel->setString("Level " + helpers::to_string(currentLevel));
    currentDamageLabel->setString(helpers::to_string(currentDamage));
    nextDamageLabel->setString("+" + helpers::to_string(nextDamage - currentDamage));
    currentCooldawnLabel->setString(helpers::to_string(currentCooldawn));
    nextCooldawnLabel->setString("-" + helpers::to_string(currentCooldawn - nextCooldawn));
    currentAdditionalLabel->setString(helpers::to_string(lifetime));
    nextAdditionalLabel->setString("+" + helpers::to_string(lifetime - nextLifetime));

    int cost = std::get<0>(ParamsManager::instance()->getSpellUpgradeParams(
                   AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(), BulletType::frostball));
    auto button = utils::findChild<Button*>(upgradeNode, "upgradeBtn");
    auto fontSize = button->getTitleLabel()->getSystemFontSize();
    auto color = button->getTitleLabel()->getTextColor();
    button->setTitleText("Upgrade \n " + helpers::to_string(cost));
    button->getTitleLabel()->setSystemFontSize(fontSize);
    button->getTitleLabel()->setTextColor(color);
    if (AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->dragon_coins < cost)
        button->setEnabled(false);
    else
        button->setEnabled(true);
    button->addTouchEventListener(CC_CALLBACK_2(SkillsScene::upgradeFrostballPressed, this));
}

void SkillsScene::setupDragonthBreathUpgrade()
{
    auto upgradeNode = utils::findChild(_currentScene, "breathBorder");
    int currentLevel = AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->dragon_breath_level;
    int nextLevel = currentLevel + 1;
    int currentDamage, nextDamage;
    float currentCooldawn, nextCooldawn;
    float currentAdditional, nextAdditional;
    float lifetime, nextLifetime;

    std::tie(currentDamage, currentCooldawn, currentAdditional, lifetime) =
            ParamsManager::instance()->getSpellParams(
                AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(),
                BulletType::dragon_breath, currentLevel);

    std::tie(nextDamage, nextCooldawn, nextAdditional, nextLifetime) =
            ParamsManager::instance()->getSpellParams(
                AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(),
                BulletType::dragon_breath, nextLevel);

    Label* levelLabel = utils::findChild<Label*>(upgradeNode, "levelLabel");
    Label* currentDamageLabel = utils::findChild<Label*>(upgradeNode, "currentDamageLabel");
    Label* nextDamageLabel = utils::findChild<Label*>(upgradeNode, "nextDamageLabel");
    Label* currentCooldawnLabel = utils::findChild<Label*>(upgradeNode, "currentCooldawnLabel");
    Label* nextCooldawnLabel = utils::findChild<Label*>(upgradeNode, "nextCooldawnLabel");
    Label* currentAdditionalLabel = utils::findChild<Label*>(upgradeNode, "currentAdditionalLabel");
    Label* nextAdditionalLabel = utils::findChild<Label*>(upgradeNode, "nextAdditionalLabel");

    levelLabel->setString("Level " + helpers::to_string(currentLevel));
    currentDamageLabel->setString(helpers::to_string(currentDamage));
    nextDamageLabel->setString("+" + helpers::to_string(nextDamage - currentDamage));
    currentCooldawnLabel->setString(helpers::to_string(currentCooldawn));
    nextCooldawnLabel->setString("-" + helpers::to_string(currentCooldawn - nextCooldawn));
    currentAdditionalLabel->setString(helpers::to_string(lifetime));
    nextAdditionalLabel->setString("+" + helpers::to_string(lifetime - nextLifetime));

    int cost = std::get<0>(ParamsManager::instance()->getSpellUpgradeParams(
                   AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(), BulletType::dragon_breath));
    auto button = utils::findChild<Button*>(upgradeNode, "upgradeBtn");
    auto fontSize = button->getTitleLabel()->getSystemFontSize();
    auto color = button->getTitleLabel()->getTextColor();
    button->setTitleText("Upgrade \n " + helpers::to_string(cost));
    button->getTitleLabel()->setSystemFontSize(fontSize);
    button->getTitleLabel()->setTextColor(color);
    if (AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->dragon_coins < cost)
        button->setEnabled(false);
    else
        button->setEnabled(true);
    button->addTouchEventListener(CC_CALLBACK_2(SkillsScene::upgradeBreathPressed, this));
}

void SkillsScene::upgradeFireballPressed(Ref *pSender, Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED)
    {
        GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
        request.SetEventKey("upgradeSkill");
        request.SetEventAttribute("type", "fireball");
        request.SetEventAttribute("price",
                                  std::get<0>(
                                  ParamsManager::instance()->getSpellUpgradeParams(
                                     AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(), BulletType::fireball)));

        std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
                std::bind(&SkillsScene::requestUpgradeResponse, this, std::placeholders::_1, std::placeholders::_2);
        request.Send(callback, 0);

        GameSparks::Api::Requests::LogEventRequest request1(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
        request1.SetEventKey("addExp");
        request1.SetEventAttribute("amount",
                                  std::get<1>(
                                  ParamsManager::instance()->getSpellUpgradeParams(
                                     AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(), BulletType::fireball)));

        std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback1 =
                std::bind(&SkillsScene::requestUpgradeExpResponse, this, std::placeholders::_1, std::placeholders::_2);
        request1.Send(callback1, 0);
    }
}

void SkillsScene::upgradeFrostballPressed(Ref *pSender, Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED)
    {
        GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
        request.SetEventKey("upgradeSkill");
        request.SetEventAttribute("type", "frostball");
        request.SetEventAttribute("price",
                                  std::get<0>(
                                  ParamsManager::instance()->getSpellUpgradeParams(
                                     AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(), BulletType::frostball)));

        std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
                std::bind(&SkillsScene::requestUpgradeResponse, this, std::placeholders::_1, std::placeholders::_2);
        request.Send(callback, 0);

        GameSparks::Api::Requests::LogEventRequest request1(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
        request1.SetEventKey("addExp");
        request1.SetEventAttribute("amount",
                                  std::get<1>(
                                  ParamsManager::instance()->getSpellUpgradeParams(
                                     AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(), BulletType::frostball)));

        std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback1 =
                std::bind(&SkillsScene::requestUpgradeExpResponse, this, std::placeholders::_1, std::placeholders::_2);
        request1.Send(callback1, 0);
    }
}

void SkillsScene::upgradeBreathPressed(Ref *pSender, Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED)
    {
        GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
        request.SetEventKey("upgradeSkill");
        request.SetEventAttribute("type", "dragonthBreath");
        request.SetEventAttribute("price",
                                  std::get<0>(
                                  ParamsManager::instance()->getSpellUpgradeParams(
                                     AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(), BulletType::dragon_breath)));

        std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
                std::bind(&SkillsScene::requestUpgradeResponse, this, std::placeholders::_1, std::placeholders::_2);
        request.Send(callback, 0);

        GameSparks::Api::Requests::LogEventRequest request1(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
        request1.SetEventKey("addExp");
        request1.SetEventAttribute("amount",
                                  std::get<1>(
                                  ParamsManager::instance()->getSpellUpgradeParams(
                                     AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(), BulletType::dragon_breath)));

        std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback1 =
                std::bind(&SkillsScene::requestUpgradeExpResponse, this, std::placeholders::_1, std::placeholders::_2);
        request1.Send(callback1, 0);
    }
}
