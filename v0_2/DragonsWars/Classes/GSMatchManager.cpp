#include "GSMatchManager.h"
#include "GameSparks/GS.h"
#include "AppDelegate.h"
#include "RTDataHelper.h"

#include <iostream>
#include <functional>

GSMatchManager::GSMatchManager()
{
    setUpListeners();
}

GSMatchManager::~GSMatchManager()
{
    for (int i = 0; i < _frostTeam.size(); ++i)
    {
        delete _frostTeam[i];
        _frostTeam[i] = nullptr;
    }

    _frostTeam.clear();

    for (int i = 0; i < _fireTeam.size(); ++i)
    {
        delete _fireTeam[i];
        _fireTeam[i] = nullptr;
    }

    _fireTeam.clear();
}

GSMatchStatus GSMatchManager::getStatus() const
{
    return _status;
}

void GSMatchManager::sendMatchmakingRequest(GSMatchType type)
{
    GameSparks::Api::Requests::MatchmakingRequest makeMatch(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
    _currentMatchType = type;
    if (type == GSMatchType::one_by_one)
        makeMatch.SetMatchShortCode("Test");
    else
        makeMatch.SetMatchShortCode("Test4_4");
    makeMatch.SetSkill(1);

    std::function<void(GS &, const GameSparks::Api::Responses::MatchmakingResponse) > callback =
            std::bind(&GSMatchManager::matchmakingRequestResponse, this, std::placeholders::_1, std::placeholders::_2);
    makeMatch.Send(callback, 2);
    cocos2d::log("send request");
    _status = GSMatchStatus::initialising;
}

void GSMatchManager::matchmakingRequestResponse(GS &,
                                                const GameSparks::Api::Responses::MatchmakingResponse &response)
{
    if (response.GetHasErrors())
        std::cerr << "!!! Error in Response:" << response.GetErrors().GetValue().GetJSON() << std::endl;
    else
        cocos2d::log("request response");
}

void GSMatchManager::sendPlayerPos(cocos2d::Vec2 pos, float angle)
{
    GameSparks::RT::RTData data;
    data.SetInt(1, (int)pos.x);
    data.SetInt(2, (int)pos.y);
    data.SetInt(3, (int)angle);
    if (AppDelegate::sharedAppDelegate()->getGSManager().m_rtSession->Session)
        AppDelegate::sharedAppDelegate()->getGSManager().m_rtSession->Session->SendRTData
                (50, GameSparks::RT::GameSparksRT::DeliveryIntent::UNRELIABLE_SEQUENCED, data, {});
}

void GSMatchManager::fireBullet(Vec2 pos, int angle, int bulletType, int bulletLevel)
{
    GameSparks::RT::RTData data;
    data.SetInt(1, (int)pos.x);
    data.SetInt(2, (int)pos.y);
    data.SetInt(3, (int)angle);
    data.SetInt(4, bulletType);
    data.SetInt(5, bulletLevel);
    if (AppDelegate::sharedAppDelegate()->getGSManager().m_rtSession->Session)
        AppDelegate::sharedAppDelegate()->getGSManager().m_rtSession->Session->SendRTData
                (51, GameSparks::RT::GameSparksRT::DeliveryIntent::UNRELIABLE_SEQUENCED, data, {});
}

void GSMatchManager::sendPlayerStatus(bool isDie)
{
    GameSparks::RT::RTData data;
    data.SetInt(1, isDie ? 1 : 0);
    if (AppDelegate::sharedAppDelegate()->getGSManager().m_rtSession->Session)
        AppDelegate::sharedAppDelegate()->getGSManager().m_rtSession->Session->SendRTData
                (52, GameSparks::RT::GameSparksRT::DeliveryIntent::UNRELIABLE_SEQUENCED, data, {});
}

int GSMatchManager::getPendingPlayersCount()
{
    return _pendingPlayersCount;
}

std::vector<PlayerData*> GSMatchManager::getFireTeam() const
{
    return _fireTeam;
}

std::vector<PlayerData*> GSMatchManager::getFrostTeam() const
{
    return _frostTeam;
}

void GSMatchManager::cleanTeams()
{
    for (auto it = _frostTeam.begin() ; it != _frostTeam.end(); ++it)
        delete (*it);
    _frostTeam.clear();

    for (auto it = _fireTeam.begin() ; it != _fireTeam.end(); ++it)
        delete (*it);
    _fireTeam.clear();
}

void GSMatchManager::setUpListeners()
{
    AppDelegate::sharedAppDelegate()->getGSManager().m_gs->SetMessageListener<MatchFoundMessage>([&](GS& gs, const MatchFoundMessage& message) {
        matchFoundResponce(gs, message); });
    AppDelegate::sharedAppDelegate()->getGSManager().m_gs->SetMessageListener<MatchNotFoundMessage>([&](GS& gs, const MatchNotFoundMessage& message) {
        matchNotFoundResponce(gs, message); });
    AppDelegate::sharedAppDelegate()->getGSManager().m_gs->SetMessageListener<MatchUpdatedMessage>([&](GS& gs, const MatchUpdatedMessage& message) {
        matchUpdateMessage(gs, message); });
}

void GSMatchManager::matchFoundResponce(GS &gs, const GameSparks::Api::Messages::MatchFoundMessage &message)
{
    cocos2d::log("MatchFoundMessage: %s", message.GetJSONString().c_str());
    std::stringstream stringPort;
    stringPort << message.GetPort().GetValue();
    AppDelegate::sharedAppDelegate()->getGSManager().m_rtSession.reset(new GameSession(
        message.GetAccessToken().GetValue(),
        message.GetHost().GetValue(),
        stringPort.str()
    ));

    setupMatchTeams(message);
    _status = GSMatchStatus::initialised;
}

void GSMatchManager::matchNotFoundResponce(GS &gs, const MatchNotFoundMessage &message)
{
    cocos2d::log("MatchNotFoundMessage: %s", message.GetJSONString().c_str());
    sendMatchmakingRequest(_currentMatchType);
}

void GSMatchManager::matchUpdateMessage(GS& gs, const MatchUpdatedMessage& message)
{
    _pendingPlayersCount = message.GetParticipants().size();
    cocos2d::log("MatchUpdatedMessage: %s", message.GetJSONString().c_str());
}

void GSMatchManager::setupMatchTeams(const MatchFoundMessage &message)
{
    auto participants = message.GetParticipants();

    auto sortRuleLambda = [] (const GameSparks::Api::Types::Participant& s1, const GameSparks::Api::Types::Participant& s2) -> bool
    {
       if (s1.GetScriptData().GetValue().GetInt("wins").GetValue() > s2.GetScriptData().GetValue().GetInt("wins").GetValue())
           return true;
       else if (s1.GetId().GetValue().compare(s1.GetId().GetValue()) > 0)
           return true;

       return false;
    };
    std::sort(participants.begin(), participants.end(), sortRuleLambda);

    for (int i = 0; i < participants.size() / 2 - (participants.size() / 2) % 2; ++i)
    {
        PlayerData* strongPlayer = new PlayerData();
        PlayerData* weakPlayer = new PlayerData();

        weakPlayer->id = participants[i].GetId().GetValue();
        weakPlayer->level = participants[i].GetScriptData().GetValue().GetInt("level").GetValue();
        weakPlayer->wins = participants[i].GetScriptData().GetValue().GetInt("wins").GetValue();
        weakPlayer->kills = participants[i].GetScriptData().GetValue().GetInt("kills").GetValue();
        weakPlayer->name = participants[i].GetScriptData().GetValue().GetString("name").GetValue();
        weakPlayer->health = participants[i].GetScriptData().GetValue().GetInt("health").GetValue();
        weakPlayer->fireball_level = participants[i].GetScriptData().GetValue().GetInt("fireball_level").GetValue();
        weakPlayer->frostball_level = participants[i].GetScriptData().GetValue().GetInt("frostball_level").GetValue();
        weakPlayer->dragon_breath_level = participants[i].GetScriptData().GetValue().GetInt("dragon_breath_level").GetValue();
        weakPlayer->skin_number = participants[i].GetScriptData().GetValue().GetInt("skin_number").GetValue();
        weakPlayer->peerId = participants[i].GetPeerId().GetValue();

        strongPlayer->id = participants[participants.size() - i - 1].GetId().GetValue();
        strongPlayer->level = participants[participants.size() - i - 1].GetScriptData().GetValue().GetInt("level").GetValue();
        strongPlayer->wins = participants[participants.size() - i - 1].GetScriptData().GetValue().GetInt("wins").GetValue();
        strongPlayer->kills = participants[participants.size() - i - 1].GetScriptData().GetValue().GetInt("kills").GetValue();
        strongPlayer->name = participants[participants.size() - i - 1].GetScriptData().GetValue().GetString("name").GetValue();
        strongPlayer->health = participants[participants.size() - i - 1].GetScriptData().GetValue().GetInt("health").GetValue();
        strongPlayer->fireball_level = participants[participants.size() - i - 1].GetScriptData().GetValue().GetInt("fireball_level").GetValue();
        strongPlayer->frostball_level = participants[participants.size() - i - 1].GetScriptData().GetValue().GetInt("frostball_level").GetValue();
        strongPlayer->dragon_breath_level = participants[participants.size() - i - 1].GetScriptData().GetValue().GetInt("dragon_breath_level").GetValue();
        strongPlayer->skin_number = participants[participants.size() - i - 1].GetScriptData().GetValue().GetInt("skin_number").GetValue();
        strongPlayer->peerId = participants[participants.size() - i - 1].GetPeerId().GetValue();

        if (i % 2 == 0)
        {
            _fireTeam.push_back(strongPlayer);
            _fireTeam.push_back(weakPlayer);
        }
        else
        {
            _frostTeam.push_back(strongPlayer);
            _frostTeam.push_back(weakPlayer);
        }
    }

    if ((participants.size() / 2) % 2)
    {
        PlayerData* firePlayer = new PlayerData();;
        PlayerData* frostPlayer = new PlayerData();;

        frostPlayer->id = participants[participants.size() / 2 - 1].GetId().GetValue();
        frostPlayer->level = participants[participants.size() / 2 - 1].GetScriptData().GetValue().GetInt("level").GetValue();
        frostPlayer->wins = participants[participants.size() / 2 - 1].GetScriptData().GetValue().GetInt("wins").GetValue();
        frostPlayer->kills = participants[participants.size() / 2 - 1].GetScriptData().GetValue().GetInt("kills").GetValue();
        frostPlayer->name = participants[participants.size() / 2 - 1].GetScriptData().GetValue().GetString("name").GetValue();
        frostPlayer->health = participants[participants.size() / 2 - 1].GetScriptData().GetValue().GetInt("health").GetValue();
        frostPlayer->fireball_level = participants[participants.size() / 2 - 1].GetScriptData().GetValue().GetInt("fireball_level").GetValue();
        frostPlayer->frostball_level = participants[participants.size() / 2 - 1].GetScriptData().GetValue().GetInt("frostball_level").GetValue();
        frostPlayer->dragon_breath_level = participants[participants.size() / 2 - 1].GetScriptData().GetValue().GetInt("dragon_breath_level").GetValue();
        frostPlayer->skin_number = participants[participants.size() / 2 - 1].GetScriptData().GetValue().GetInt("skin_number").GetValue();
        frostPlayer->peerId = participants[participants.size() / 2 - 1].GetPeerId().GetValue();

        firePlayer->id = participants[participants.size() / 2].GetId().GetValue();
        firePlayer->level = participants[participants.size() / 2].GetScriptData().GetValue().GetInt("level").GetValue();
        firePlayer->wins = participants[participants.size() / 2].GetScriptData().GetValue().GetInt("wins").GetValue();
        firePlayer->kills = participants[participants.size() / 2].GetScriptData().GetValue().GetInt("kills").GetValue();
        firePlayer->name = participants[participants.size() / 2].GetScriptData().GetValue().GetString("name").GetValue();
        firePlayer->health = participants[participants.size() / 2].GetScriptData().GetValue().GetInt("health").GetValue();
        firePlayer->fireball_level = participants[participants.size() / 2].GetScriptData().GetValue().GetInt("fireball_level").GetValue();
        firePlayer->frostball_level = participants[participants.size() / 2].GetScriptData().GetValue().GetInt("frostball_level").GetValue();
        firePlayer->dragon_breath_level = participants[participants.size() / 2].GetScriptData().GetValue().GetInt("dragon_breath_level").GetValue();
        firePlayer->skin_number = participants[participants.size() / 2].GetScriptData().GetValue().GetInt("skin_number").GetValue();
        firePlayer->peerId = participants[participants.size() / 2].GetPeerId().GetValue();

        _fireTeam.push_back(firePlayer);
        _frostTeam.push_back(frostPlayer);
    }
}

