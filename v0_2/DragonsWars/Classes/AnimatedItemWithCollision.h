#pragma once

#include "cocos2d.h"

using namespace cocos2d;

enum ItemWithCollisionType {cloud1 = 0, cloud2, cloud3};

class AnimatedItemWithCollision: public Sprite
{
public:
    static AnimatedItemWithCollision* createObject(std::string id, ItemWithCollisionType type);

    int getDamage() const;
    std::string getId() const;

protected:
private:
    AnimatedItemWithCollision(std::string id, SpriteFrame *frame);
    ~AnimatedItemWithCollision() = default;

    std::string _id;
};
