#pragma once

#include "cocos2d.h"
#include <memory>
#include <GameSparks/generated/GSRequests.h>
#include <GameSparks/generated/GSMessages.h>

#include "GameSession.h"
#include "Bullet.h"

#include <tuple>

using namespace GameSparks::Api::Messages;

class PlayerBattleData
{
public:
    PlayerBattleData() = default;
    ~PlayerBattleData() = default;

    int kills;
    int dealDamage;
    int team;
};

class PlayerData
{
public:
    PlayerData();
    ~PlayerData() = default;

    PlayerBattleData* getBattleData() const;
    void clearMatchData();
    void initBattleData(int team);

    int dragon_coins;
    int expirience;
    int health;
    int kills;
    int level;
    int level_exp_needed;
    int number_of_deaths;
    int skin_number;
    int total_damage;
    int wins;
    int fireball_level;
    int frostball_level;
    int dragon_breath_level;
    std::string name;
    std::string id;
    std::vector<int> openedSkins;
    int earned_coins;
    int num_of_loses;
    int recieved_damage;

    int peerId;
private:
    PlayerBattleData* _battleData;
};


class PlayerDataManager
{
public:
    PlayerDataManager();
    ~PlayerDataManager() = default;

    const std::unique_ptr<PlayerData>& getData() const;

    void requestPlayerData();
    void requestSetNewPlayerData();
    bool isDataLoaded() const;

    void requestPlayerDataResponse(GameSparks::Core::GS& gsInstance,
                               const GameSparks::Api::Responses::LogEventResponse response);
    void requestSetNewPlayerDataResponse(GameSparks::Core::GS& gsInstance,
                               const GameSparks::Api::Responses::LogEventResponse response);

    std::tuple<int, float, float> getSpellDamage(BulletType spellType, int spellLevel);

private:
    bool _dataLoaded = false;
    std::unique_ptr<PlayerData> _data;
};
