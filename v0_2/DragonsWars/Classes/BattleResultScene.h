#pragma once

#include "cocos2d.h"

#include <GameSparks/generated/GSRequests.h>
#include <GameSparks/generated/GSMessages.h>

using namespace GameSparks::Api::Messages;

using namespace cocos2d;

class PlayerData;
class BattleResultScene : public cocos2d::Scene
{
public:

    static Scene* createScene(std::pair<int, int> battleScore, std::pair<int, int> battleDD);
    
    virtual bool init(std::pair<int, int> battleScore, std::pair<int, int> battleDD);
    
    void requestBattleResultDataResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response);
    void requestExpChangeResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response);
    void requestCoinsChangeResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response);

protected:
    BattleResultScene();
    ~BattleResultScene() = default;

    void setupPlayersData();
    void requestBattleResultDataChange(bool isPlayerWin);
    void requestExpChange(int amount);
    void requestCoinsChange(int amount);

    Scene* _currentScene = nullptr;
    int _winnerTeam = -1;
    PlayerData* _currentPlayerData;
};
