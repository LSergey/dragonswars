#pragma once

#include "cocos2d.h"
#include "GameSession.h"
#include <GameSparks/generated/GSRequests.h>
#include <GameSparks/generated/GSMessages.h>
#include "PlayerDataManager.h"

using namespace cocos2d;
using namespace GameSparks::Api::Messages;

enum class GSMatchStatus {not_created = 0, initialising, initialised, finished};
enum GSMatchType {one_by_one = 0, two_by_two};

class GSMatchManager
{
public:
    GSMatchManager();
    ~GSMatchManager();

    GSMatchStatus getStatus() const;

    void sendMatchmakingRequest(GSMatchType type);
    void matchmakingRequestResponse(GameSparks::Core::GS&,
                                     const GameSparks::Api::Responses::MatchmakingResponse& response);

    void sendPlayerPos(Vec2 pos, float angle);
    void fireBullet(Vec2 pos, int angle, int bulletType, int bulletLevel);
    void sendPlayerStatus(bool isDie);
    int getPendingPlayersCount();

    std::vector<PlayerData*> getFireTeam() const;
    std::vector<PlayerData*> getFrostTeam() const;
    void cleanTeams();

private:
    GSMatchStatus _status = GSMatchStatus::not_created;

    void setUpListeners();
    void matchFoundResponce(GS& gs, const MatchFoundMessage& message);
    void matchNotFoundResponce(GS& gs, const MatchNotFoundMessage& message);
    void matchUpdateMessage(GS& gs, const MatchUpdatedMessage& message);
    void setupMatchTeams(const GameSparks::Api::Messages::MatchFoundMessage &message);

    int _pendingPlayersCount = 0;
    std::vector<PlayerData*> _fireTeam;
    std::vector<PlayerData*> _frostTeam;
    GSMatchType _currentMatchType;
};
