#include "MainScene.h"
#include "platform/CCFileUtils.h"
#include "reader/CreatorReader.h"
#include "AppDelegate.h"
#include "ui/CocosGUI.h"
#include "SearchMatchScene.h"
#include "Helpers.h"
#include "SkinsScene.h"
#include "SkillsScene.h"
#include "PlayerProfileScene.h"
#include "GSMatchManager.h"

Scene* MainScene::createScene()
{
    MainScene *pRet = new(std::nothrow) MainScene();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

bool MainScene::init() {

    if ( !Scene::init() ) {
        return false;
    }

    scheduleUpdate();

    setupPlayerData();
    setupIconsDescriptions();

    auto button1x1 = utils::findChild<ui::Button*>(_currentScene, "startBtn1x1");
    button1x1->addClickEventListener([this](Ref*) {
        this->getEventDispatcher()->removeEventListener(listener);
        AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager().get()->sendMatchmakingRequest(GSMatchType::one_by_one);
        Director::getInstance()->replaceScene(SearchMatchScene::createScene(GameMode::oneXone));
    });

    auto button = utils::findChild<ui::Button*>(_currentScene, "startBtn2x2");
    button->addClickEventListener([this](Ref*) {
        this->getEventDispatcher()->removeEventListener(listener);
        AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager().get()->sendMatchmakingRequest(GSMatchType::two_by_two);
        Director::getInstance()->replaceScene(SearchMatchScene::createScene(GameMode::twoXtwo));
    });

    auto button_skin = utils::findChild<ui::Button*>(_currentScene, "skinBtn");
    button_skin->addClickEventListener([this](Ref*) {
//        auto layer = Layer::create();
//        layer->addChild(SkinsScene::createScene());
//        this->_currentScene->addChild(layer);
        Director::getInstance()->pushScene(SkinsScene::createScene());
    });

    auto button_skill = utils::findChild<ui::Button*>(_currentScene, "skillsBtn");
    button_skill->addClickEventListener([](Ref*) {
        Director::getInstance()->pushScene(SkillsScene::createScene());
    });

    auto playerIconBtn = utils::findChild<ui::Button*>(_currentScene, "player_icon");
    playerIconBtn->addClickEventListener([this](Ref*) {
        if (this->_scenesLayer)
        {
            _scenesLayer->removeFromParent();
        }
        _scenesLayer = Layer::create();
        _scenesLayer->addChild(PlayerProfileScene::createScene());
        this->_currentScene->addChild(_scenesLayer);
    });


    return true;
}

void MainScene::update(float dt)
{
    setupPlayerData();
}

Scene *MainScene::getScene() const
{
    return _currentScene;
}

MainScene::MainScene()
{
    AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->requestPlayerData();

    auto reader = creator::CreatorReader::createWithFilename("creator/MainScene.ccreator");
    reader->setup();
    _currentScene = reader->getSceneGraph();
    addChild(_currentScene);
}

MainScene::~MainScene()
{

}

void MainScene::setupPlayerData()
{

    if (_firstSetup)
    {
        auto visibleSize = Director::getInstance()->getVisibleSize();
        Vec2 origin = Director::getInstance()->getVisibleOrigin();
        float shiftToBorderValue = (visibleSize.width - _currentScene->getChildByName("Canvas")->getContentSize().width ) / 2;
        auto leftAlignNode = utils::findChild<Node*>(_currentScene, "leftAlignNode");
        leftAlignNode->setPosition(Vec2(leftAlignNode->getPosition().x - shiftToBorderValue, leftAlignNode->getPosition().y));
        auto rightAlignNode = utils::findChild<Node*>(_currentScene, "rightAlignNode");
        rightAlignNode->setPosition(Vec2(rightAlignNode->getPosition().x + shiftToBorderValue, rightAlignNode->getPosition().y));
    }

    auto levelBtn = utils::findChild<Button*>(_currentScene, "levelBgBtn");
    auto fontSize = levelBtn->getTitleLabel()->getSystemFontSize();
    auto color = levelBtn->getTitleLabel()->getTextColor();
    levelBtn->setTitleText(helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->level));
    levelBtn->getTitleLabel()->setSystemFontSize(fontSize);
    levelBtn->getTitleLabel()->setTextColor(color);

    auto coins_count = utils::findChild<Label*>(_currentScene, "coins_count");
    coins_count->setString(helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->dragon_coins));
    auto name_label = utils::findChild<Label*>(_currentScene, "player_name");
    name_label->setString(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->name);
    auto exp_progress = utils::findChild<Label*>(_currentScene, "exp_progress");
    exp_progress->setString(helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->expirience) + " / " +
                            helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->level_exp_needed));
    auto expProgressBar = static_cast<LoadingBar*>(utils::findChild(_currentScene, "progressBar"));
    expProgressBar->setPercent( 100 * ((float)AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->expirience /
                               (float)AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->level_exp_needed));

    auto winsBtn = utils::findChild<Button*>(_currentScene, "winsBtn");
    fontSize = winsBtn->getTitleLabel()->getSystemFontSize();
    color = winsBtn->getTitleLabel()->getTextColor();
    winsBtn->setTitleText(helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->wins));
    winsBtn->getTitleLabel()->setSystemFontSize(fontSize);
    winsBtn->getTitleLabel()->setTextColor(color);
    winsBtn->getTitleLabel()->setPosition(Vec2(winsBtn->getContentSize().width * 1.2, 0));


    auto killsBtn = utils::findChild<Button*>(_currentScene, "killsBtn");
    fontSize = killsBtn->getTitleLabel()->getSystemFontSize();
    color = killsBtn->getTitleLabel()->getTextColor();
    killsBtn->setTitleText(helpers::to_string(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->kills));
    killsBtn->getTitleLabel()->setSystemFontSize(fontSize);
    killsBtn->getTitleLabel()->setTextColor(color);
    killsBtn->getTitleLabel()->setPosition(Vec2(killsBtn->getContentSize().width * 1.2, 0));

    _firstSetup = false;
}

void MainScene::setupIconsDescriptions()
{
    auto gold_icon_btn = utils::findChild<ui::Button*>(_currentScene, "gold_icon");
    gold_icon_btn->addClickEventListener([this, gold_icon_btn](Ref*) {
        setDescriptionText(MainScene::DescriptionType::coin);
        auto descriptionBtn = utils::findChild<Button*>(_currentScene, "iconDescription");
        descriptionBtn->setVisible(true);
        descriptionBtn->setPosition(utils::findChild<Node*>(_currentScene, "rightAlignNode")->getPosition()
                                    + gold_icon_btn->getPosition() - Vec2(0, gold_icon_btn->getContentSize().height / 2)
                                    - Vec2(descriptionBtn->getContentSize().width / 4, descriptionBtn->getContentSize().height / 2));
        descriptionBtn->addClickEventListener([descriptionBtn](Ref*) {
            descriptionBtn->setVisible(false);
        });
    });

    auto exp_icon_btn = utils::findChild<ui::Button*>(_currentScene, "levelBgBtn");
    exp_icon_btn->addClickEventListener([this, exp_icon_btn](Ref*) {
        setDescriptionText(MainScene::DescriptionType::exp);
        auto descriptionBtn = utils::findChild<Button*>(_currentScene, "iconDescription");
        descriptionBtn->setVisible(true);
        descriptionBtn->setPosition(utils::findChild<Node*>(_currentScene, "leftAlignNode")->getPosition()
                                    + exp_icon_btn->getPosition() - Vec2(0, exp_icon_btn->getContentSize().height / 2)
                                    + Vec2(descriptionBtn->getContentSize().width / 4, -descriptionBtn->getContentSize().height / 2));
        descriptionBtn->addClickEventListener([descriptionBtn](Ref*) {
            descriptionBtn->setVisible(false);
        });
    });

    auto cup_icon_btn = utils::findChild<ui::Button*>(_currentScene, "winsBtn");
    cup_icon_btn->addClickEventListener([this, cup_icon_btn](Ref*) {
        setDescriptionText(MainScene::DescriptionType::wins);
        auto descriptionBtn = utils::findChild<Button*>(_currentScene, "iconDescription");
        descriptionBtn->setVisible(true);
        descriptionBtn->setPosition(utils::findChild<Node*>(_currentScene, "leftAlignNode")->getPosition()
                                    + cup_icon_btn->getPosition() - Vec2(0, cup_icon_btn->getContentSize().height / 2)
                                    + Vec2(descriptionBtn->getContentSize().width / 4, -descriptionBtn->getContentSize().height / 2));
        descriptionBtn->addClickEventListener([descriptionBtn](Ref*) {
            descriptionBtn->setVisible(false);
        });
    });

    auto kills_icon_btn = utils::findChild<ui::Button*>(_currentScene, "killsBtn");
    kills_icon_btn->addClickEventListener([this, kills_icon_btn](Ref*) {
        setDescriptionText(MainScene::DescriptionType::kills);
        auto descriptionBtn = utils::findChild<Button*>(_currentScene, "iconDescription");
        descriptionBtn->setVisible(true);
        descriptionBtn->setPosition(utils::findChild<Node*>(_currentScene, "rightAlignNode")->getPosition()
                                    + kills_icon_btn->getPosition() - Vec2(0, kills_icon_btn->getContentSize().height / 2)
                                    - Vec2(0, descriptionBtn->getContentSize().height / 2));
        descriptionBtn->addClickEventListener([descriptionBtn](Ref*) {
            descriptionBtn->setVisible(false);
        });
    });

    listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = [this](Touch* touch, Event* event){
        if (utils::findChild<Button*>(_currentScene, "iconDescription")->isVisible())
        {
            utils::findChild<Button*>(_currentScene, "iconDescription")->setVisible(false);
            return true;
        }

        return false;
    };
    _eventDispatcher->addEventListenerWithFixedPriority(listener, -100);
}

void MainScene::setDescriptionText(MainScene::DescriptionType type)
{
    std::string text = "";
    switch (type) {
    case MainScene::DescriptionType::exp :
        text = "very long expirience\ndescriptions.\nI love DragonsWars.";
        break;
    case MainScene::DescriptionType::coin :
        text = "Donate! Donate!\n\nMore and more!!";
        break;
    case MainScene::DescriptionType::wins :
        text = "You are Legendary, bro!\nBest for the best!";
        break;
    case MainScene::DescriptionType::kills :
        text = "Kill, more and more!\nI want see blood.\nKill, kill!";
        break;
    default:
        break;
    }

    auto descriptionBtn = utils::findChild<Button*>(_currentScene, "iconDescription");
    auto fontSize = descriptionBtn->getTitleLabel()->getSystemFontSize();
    auto color = descriptionBtn->getTitleLabel()->getTextColor();
    descriptionBtn->setTitleText(text);
    descriptionBtn->getTitleLabel()->setSystemFontSize(fontSize);
    descriptionBtn->getTitleLabel()->setTextColor(color);
}
