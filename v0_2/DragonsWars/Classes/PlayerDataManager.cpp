#include "PlayerDataManager.h"
#include "AppDelegate.h"

PlayerDataManager::PlayerDataManager()
{
    _data = std::make_unique<PlayerData>();
}

const std::unique_ptr<PlayerData> &PlayerDataManager::getData() const
{
    return _data;
}

void PlayerDataManager::requestPlayerData()
{
    GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
    request.SetEventKey("getPlayerData");

    std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
            std::bind(&PlayerDataManager::requestPlayerDataResponse, this, std::placeholders::_1, std::placeholders::_2);
    request.Send(callback, 0);
}

void PlayerDataManager::requestSetNewPlayerData()
{
    GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
    request.SetEventKey("setNewPlayerData");

    std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
            std::bind(&PlayerDataManager::requestSetNewPlayerDataResponse, this, std::placeholders::_1, std::placeholders::_2);
    request.Send(callback, 0);
}

bool PlayerDataManager::isDataLoaded() const
{
    return _dataLoaded;
}

void PlayerDataManager::requestPlayerDataResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response)
{
    if (response.GetHasErrors())
    {
    }
    else
    {
        auto scriptData = response.GetBaseData().GetGSDataObject("scriptData").GetValue();

        _data->id = scriptData.GetString("id").GetValue();
        _data->name = scriptData.GetString("name").GetValue();
        _data->level = scriptData.GetInt("level").GetValue();
        _data->level_exp_needed = scriptData.GetInt("level_exp_needed").GetValue();
        _data->expirience = scriptData.GetInt("expirience").GetValue();
        _data->dragon_coins = scriptData.GetInt("dragon_coins").GetValue();
        _data->wins = scriptData.GetInt("wins").GetValue();
        _data->kills = scriptData.GetInt("kills").GetValue();
        _data->skin_number = scriptData.GetInt("skin_number").GetValue();
        _data->health = scriptData.GetInt("health").GetValue();
        _data->number_of_deaths = scriptData.GetInt("number_of_deaths").GetValue();
        _data->total_damage = scriptData.GetInt("total_damage").GetValue();
        _data->fireball_level = scriptData.GetInt("fireball_level").GetValue();
        _data->frostball_level = scriptData.GetInt("frostball_level").GetValue();
        _data->dragon_breath_level = scriptData.GetInt("dragon_breath_level").GetValue();
        _data->openedSkins = scriptData.GetIntList("opened_skins");
        _data->earned_coins = scriptData.GetInt("earned_coins").GetValue();
        _data->num_of_loses = scriptData.GetInt("num_of_loses").GetValue();
        _data->recieved_damage = scriptData.GetInt("recieved_damage").GetValue();

        _dataLoaded = true;
    }
}

void PlayerDataManager::requestSetNewPlayerDataResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response)
{
    if (response.GetHasErrors())
    {
    }
    else
    {
        requestPlayerData();
    }
}

PlayerData::PlayerData()
{
    _battleData = new PlayerBattleData();
}

PlayerBattleData *PlayerData::getBattleData() const
{
    return _battleData;
}

void PlayerData::clearMatchData()
{
    if (_battleData)
    {
        delete _battleData;
        _battleData = nullptr;
    }
}

void PlayerData::initBattleData(int team)
{
    if (_battleData)
    {
        delete _battleData;
        _battleData = nullptr;
    }
    _battleData = new PlayerBattleData();
    _battleData->team = team;
}
