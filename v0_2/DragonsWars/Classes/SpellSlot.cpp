#include "SpellSlot.h"
#include "reader/CreatorReader.h"
#include "AppDelegate.h"
#include "Helpers.h"
#include "ParamsManager.h"

SpellSlot *SpellSlot::createObject(BulletType type, PlayerData* playerData, PlayerTeamType teamType)
{
    auto reader = creator::CreatorReader::createWithFilename("creator/BulletsScene.ccreator");
    reader->setup();
    auto scene = reader->getSceneGraph(true);
    auto slotBtn = utils::findChild<Button*>(scene, "slot");

    auto slot = new SpellSlot(type, playerData, slotBtn->getTitleLabel()->getTextColor(), slotBtn->getTitleLabel()->getSystemFontSize(), teamType);

    std::string path;
    if (teamType == PlayerTeamType::fire_team)
        path = "res/fireIconBg.png";
    else
        path = "res/frostIconBg.png";
    if (slot && slot->init(path))
        {
            slot->autorelease();
            slot->initObj();
            slot->setAnchorPoint(Vec2(0.f, 0.f));
            slot->setContentSize(slotBtn->getContentSize());
            slot->setTitleFontSize(slotBtn->getTitleLabel()->getSystemFontSize());
            slot->getTitleLabel()->setVisible(false);

            return slot;
        }

        CC_SAFE_DELETE(slot);
        return nullptr;
}

float SpellSlot::getColldown() const
{
    return _cooldown;
}

void SpellSlot::activateCooldown()
{
    this->setTitleText(helpers::to_string(this->getColldown()));
    this->getTitleLabel()->setSystemFontSize(_fontSize);
    this->getTitleLabel()->setTextColor(_color);
    this->getTitleLabel()->setVisible(true);
    _isCooldown = true;
    _cooldownTimer = _cooldown;
    _icon->setVisible(false);

    this->schedule(schedule_selector(SpellSlot::updateTimer), 0.1f);
}

bool SpellSlot::isCooldown() const
{
    return _isCooldown;
}

void SpellSlot::initObj()
{
    std::string path;
    switch (_slotType)
    {
    case BulletType::fireball :
        path = "res/fireballIcon.png";
        break;
    case BulletType::frostball :
        path = "res/frostballIcon.png";
        break;
    case BulletType::dragon_breath :
        if (_teamType == PlayerTeamType::frost_team)
            path = "res/frostBreathIcon.png";
        else
            path = "res/fireBreathIcon.png";
        break;
    }
    _icon = Sprite::create();
    _icon->initWithFile(path);
    _icon->setAnchorPoint(Vec2(0.5, 0.5));
    _icon->setPosition(this->getContentSize().width / 2, this->getContentSize().height / 2);
    this->addChild(_icon);
}

SpellSlot::SpellSlot(BulletType type, PlayerData* playerData, Color4B labelColor, float fontSize, PlayerTeamType teamType)
    : _slotType(type),
      _playerData(playerData),
      _color(labelColor),
      _fontSize(fontSize),
      _teamType(teamType)
{
    int spellLevel;
    switch (type)
    {
    case BulletType::fireball :
        spellLevel = playerData->fireball_level;
        break;
    case BulletType::frostball :
        spellLevel = playerData->frostball_level;
        break;
    case BulletType::dragon_breath :
        spellLevel = playerData->dragon_breath_level;
        break;
    }

    _cooldown = std::get<1>(
                ParamsManager::instance()->getSpellBattleParams(
                    AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData().get(), type));
}

void SpellSlot::updateTimer(float)
{
    _cooldownTimer -= 0.1f;
    this->setTitleText(helpers::to_string_with_precision(_cooldownTimer, 1));
    this->getTitleLabel()->setSystemFontSize(_fontSize);
    this->getTitleLabel()->setTextColor(_color);

    if (_cooldownTimer < 0.1f)
    {
        _isCooldown = false;
        this->unschedule(schedule_selector(SpellSlot::updateTimer));
        this->getTitleLabel()->setVisible(false);
        _icon->setVisible(true);
    }
}
