#include "BattleResultScene.h"
#include "platform/CCFileUtils.h"
#include "reader/CreatorReader.h"
#include "AppDelegate.h"
#include "ui/CocosGUI.h"
#include "Helpers.h"
#include "MainScene.h"

Scene* BattleResultScene::createScene(std::pair<int, int> battleScore, std::pair<int, int> battleDD)
{
    BattleResultScene *pRet = new(std::nothrow) BattleResultScene();
    if (pRet && pRet->init(battleScore, battleDD))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

bool BattleResultScene::init(std::pair<int, int> battleScore, std::pair<int, int> battleDD)
{
    if ( !Scene::init() ) {
        return false;
    }

    auto back_btn = utils::findChild<ui::Button*>(_currentScene, "homeBtn");
    back_btn->addClickEventListener([this](Ref*) {
        AppDelegate::sharedAppDelegate()->getGSManager().m_rtSession->Session->Stop();
        AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->cleanTeams();
        Director::getInstance()->replaceScene(MainScene::createScene());
    });

    utils::findChild<Label*>(_currentScene, "resultNumberFireTeam")->setString(helpers::to_string(battleScore.first));
    utils::findChild<Label*>(_currentScene, "resultNumberFrostTeam")->setString(helpers::to_string(battleScore.second));
    utils::findChild<Label*>(_currentScene, "ddFireTeam")->setString(helpers::to_string(battleDD.first));
    utils::findChild<Label*>(_currentScene, "ddFrostTeam")->setString(helpers::to_string(battleDD.second));

    if ((battleScore.first > battleScore.second) ||
            (battleScore.first == battleScore.second && battleDD.first > battleDD.second))
    {
//        utils::findChild<Label*>(_currentScene, "winnerLabel")->setString("Winner - Fire team");
        _winnerTeam = 0;
    }
    else if ((battleScore.first < battleScore.second) ||
                (battleScore.first == battleScore.second && battleDD.first < battleDD.second))
    {
//        utils::findChild<Label*>(_currentScene, "winnerLabel")->setString("Winner - Frost team");
        _winnerTeam = 1;
    }
    else
        utils::findChild<Label*>(_currentScene, "winnerLabel")->setString("Draw");

    auto fireTeam = AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getFireTeam();
    auto frostTeam = AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getFrostTeam();
    for (int i = 0; i < fireTeam.size(); ++i)
    {
        if (fireTeam[i]->id == AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->id)
            _currentPlayerData = fireTeam[i];
    }
    for (int i = 0; i < frostTeam.size(); ++i)
    {
        if (frostTeam[i]->id == AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->id)
            _currentPlayerData = frostTeam[i];
    }

    requestBattleResultDataChange(_currentPlayerData->getBattleData()->team == _winnerTeam);

    setupPlayersData();

    return true;
}

void BattleResultScene::requestBattleResultDataResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response)
{
    if (response.GetHasErrors())
    {
    }
    else
    {

    }
}

void BattleResultScene::requestExpChangeResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response)
{
    if (response.GetHasErrors())
    {
    }
    else
    {

    }
}

void BattleResultScene::requestCoinsChangeResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response)
{
    if (response.GetHasErrors())
    {
    }
    else
    {

    }
}

void BattleResultScene::setupPlayersData()
{
    auto fireTeam = AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getFireTeam();
    auto frostTeam = AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getFrostTeam();

    std::sort(std::begin(fireTeam), std::end(fireTeam),
              [](PlayerData* a, PlayerData* b) {
        if (a->getBattleData()->kills == b->getBattleData()->kills)
            return a->getBattleData()->dealDamage > b->getBattleData()->dealDamage;
        else
            return a->getBattleData()->kills > b->getBattleData()->kills;
    });

    std::sort(std::begin(frostTeam), std::end(frostTeam),
              [](PlayerData* a, PlayerData* b) {
        if (a->getBattleData()->kills == b->getBattleData()->kills)
            return a->getBattleData()->dealDamage > b->getBattleData()->dealDamage;
        else
            return a->getBattleData()->kills > b->getBattleData()->kills;
    });

    auto node1 = utils::findChild<Node*>(_currentScene, "player1ResultNode");
    auto node2 = utils::findChild<Node*>(_currentScene, "player2ResultNode");
    auto node3 = utils::findChild<Node*>(_currentScene, "player3ResultNode");
    auto node4 = utils::findChild<Node*>(_currentScene, "player4ResultNode");
    std::vector<Node*> nodesFire {node1, node2};
    std::vector<Node*> nodesFrost {node3, node4};
    std::for_each(nodesFire.begin(), nodesFire.end(), [](Node* node) {node->setVisible(false);});
    std::for_each(nodesFrost.begin(), nodesFrost.end(), [](Node* node) {node->setVisible(false);});

    for (int i = 0; i < fireTeam.size(); ++i)
    {
        auto playerData = fireTeam[i];
        auto node = nodesFire[i];

        node->setVisible(true);
        utils::findChild<Label*>(node, "playerName")->setString(playerData->name);
        utils::findChild<Label*>(node, "kills")->setString(helpers::to_string(playerData->getBattleData()->kills));
        utils::findChild<Label*>(node, "dd")->setString(helpers::to_string(playerData->getBattleData()->dealDamage));
        utils::findChild<Label*>(node, "number")->setString(helpers::to_string(playerData->level));
    }

    for (int i = 0; i < frostTeam.size(); ++i)
    {
        auto playerData = frostTeam[i];
        auto node = nodesFrost[i];

        node->setVisible(true);
        utils::findChild<Label*>(node, "playerName")->setString(playerData->name);
        utils::findChild<Label*>(node, "kills")->setString(helpers::to_string(playerData->getBattleData()->kills));
        utils::findChild<Label*>(node, "dd")->setString(helpers::to_string(playerData->getBattleData()->dealDamage));
        utils::findChild<Label*>(node, "number")->setString(helpers::to_string(playerData->level));
    }

    if (_currentPlayerData->getBattleData()->team == _winnerTeam)
    {
        utils::findChild<Label*>(_currentScene, "goldNumLabel")->setString(helpers::to_string(1));
        requestCoinsChange(1);
        int expIncrease = _currentPlayerData->getBattleData()->kills * 2 + 20;
        utils::findChild<Label*>(_currentScene, "expirienceNumLabel")->setString(helpers::to_string(expIncrease));
        requestExpChange(expIncrease);
    }
    else
    {
        utils::findChild<Label*>(_currentScene, "goldNumLabel")->setVisible(false);
        utils::findChild<Node*>(_currentScene, "gold_icon")->setVisible(false);
        int expIncrease = _currentPlayerData->getBattleData()->kills;
        utils::findChild<Label*>(_currentScene, "expirienceNumLabel")->setString(helpers::to_string(_currentPlayerData->getBattleData()->kills));
        requestExpChange(expIncrease);
    }
}

void BattleResultScene::requestBattleResultDataChange(bool isPlayerWin)
{
    GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
    request.SetEventKey("addWinLose");
    if (isPlayerWin)
        request.SetEventAttribute("type", "wins");
    else
        request.SetEventAttribute("type", "loses");

    std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
            std::bind(&BattleResultScene::requestBattleResultDataResponse, this, std::placeholders::_1, std::placeholders::_2);
    request.Send(callback, 0);
}

void BattleResultScene::requestExpChange(int amount)
{
    GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
    request.SetEventKey("addExp");
    request.SetEventAttribute("amount", amount);

    std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
            std::bind(&BattleResultScene::requestExpChangeResponse, this, std::placeholders::_1, std::placeholders::_2);
    request.Send(callback, 0);
}

void BattleResultScene::requestCoinsChange(int amount)
{
    GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
    request.SetEventKey("addCoins");
    request.SetEventAttribute("amount", amount);

    std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
            std::bind(&BattleResultScene::requestCoinsChangeResponse, this, std::placeholders::_1, std::placeholders::_2);
    request.Send(callback, 0);
}

BattleResultScene::BattleResultScene()
{
    auto reader = creator::CreatorReader::createWithFilename("creator/BattleResultScene.ccreator");
    reader->setup();
    _currentScene = reader->getSceneGraph();
    addChild(_currentScene);
}
