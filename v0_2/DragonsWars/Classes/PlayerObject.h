#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace ui;

enum class PlayerTeamType {fire_team = 0, frost_team};

class PlayerData;
class SmalMapDot;
class Bullet;
class PlayerObject : public Sprite
{
public:
    static PlayerObject* createObject(Vec2 spawnGlobalPos, PlayerTeamType teamType,
                                      PlayerData* playerData, Sprite *bg = nullptr, SmalMapDot* dot = nullptr);
    bool init() override;
    void update(float dt);
    void respawn(bool withBgShift);
    void setDefaultValues();
    void updatePositionWithBgShift(float angle);
    void updatePosition(Vec2 newPos, float angle);
    void updatePositionWhenStatic(Vec2 globalPosition);
    void updatePosition();

    int getHealth() const;
    float getPrevAngleDegree() const;
    PlayerData *getPlayerData() const;
    PlayerTeamType getPlayerTeamType() const;
    void setPrevAngleDegree(float angle);
    void setPrevAngleRad(float angle);
    void setPlayerData(PlayerData* data);
    void setPlayerTeamType(PlayerTeamType teamType);
    void setSpawnGlobalPos(Vec2 globalPos);

    void bulletDamage(Bullet *bullet);
    void mapItemDamage(std::string id, int damage);
    bool isAfterRespawn() const;

    bool isCanBeDamagedByAreaObject(std::string id) const;
    bool isCanBeDamagedByBullet(Bullet* bullet) const;
    void startDamageByAreaObject(std::string id, float tickLenght);
    void updateCanBeDamagedObjects(float dt);

    Vec2 getSpellPos(int type) const;
    void setSpellsPos(Vec2 fire, Vec2 frost, Vec2 breath);

private:
    PlayerObject(Vec2 spawnGlobalPos, PlayerTeamType teamType,
                 PlayerData* playerData, Sprite *bg, SmalMapDot* dot);
    ~PlayerObject() = default;
    void respawnFinished(float dt);
    void updateAfterRespawn();

    Vec2 getRotationChildPoint(Vec2 childZeroAnglePos) const;

    bool _startMove = false;
    bool _isAfterRespawn = false;
    int _health;
    float _prevAngleDegree;
    float _distanceCoeficient;
    Vec2 _startBgPos;
    float _startRotationAngle;

    Vec2 _spawnGlobalPos;
    PlayerTeamType _teamType;
    PlayerData* _playerData = nullptr;
    Sprite* _bg = nullptr;
    SmalMapDot* _dot = nullptr;
    bool _isCurrent;

    std::vector<std::pair<std::string, float> > _canBeDamagedByAreaObjects;

    Vec2 _fireballPos, _frostbalPos, _breathPos;
};
