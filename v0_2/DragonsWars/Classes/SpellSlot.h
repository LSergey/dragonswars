#pragma once

#include "cocos2d.h"
#include "PlayerObject.h"
#include "Bullet.h"

using namespace cocos2d;

class PlayerData;
class SpellSlot: public Button
{
public:
    static SpellSlot* createObject(BulletType type, PlayerData* playerData, PlayerTeamType teamType);

    float getColldown() const;
    void activateCooldown();
    bool isCooldown() const;

    virtual void initObj();

private:
    SpellSlot(BulletType type, PlayerData* playerData, Color4B labelColor, float fontSize, PlayerTeamType teamType);
    ~SpellSlot() = default;

    void updateTimer(float);

    BulletType _slotType;
    PlayerTeamType _teamType;
    PlayerData* _playerData = nullptr;
    Sprite* _icon = nullptr;
    float _cooldown = -1.f;
    float _cooldownTimer;
    bool _isCooldown = false;
    Color4B _color;
    float _fontSize;
};
