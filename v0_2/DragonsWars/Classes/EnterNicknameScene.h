#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

#include <GameSparks/generated/GSRequests.h>
#include <GameSparks/generated/GSMessages.h>

using namespace GameSparks::Api::Messages;

using namespace cocos2d;
using namespace cocos2d::ui;

class EnterNicknameScene : public cocos2d::Scene, public EditBoxDelegate
{
public:
    static Scene* createScene();
    
    virtual bool init();
    //virtual void update(float dt);

    virtual void editBoxReturn(EditBox* editBox);
    void requestUseResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response);
private:
    EnterNicknameScene();
    ~EnterNicknameScene() = default;

    Scene* _currentScene = nullptr;
    EditBox* _editbox = nullptr;
};
