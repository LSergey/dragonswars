#include "ReceivedDamageSprite.h"
#include "Helpers.h"

ReceivedDamageSprite *ReceivedDamageSprite::createObject(int num, cocos2d::Vec2 pos)
{
    auto sprite = new ReceivedDamageSprite(num, pos);

    if (sprite && sprite->init())
    {
        sprite->autorelease();

        sprite->scheduleUpdate();

        return sprite;
    }

    CC_SAFE_DELETE(sprite);
    return nullptr;
}

void ReceivedDamageSprite::update(float dt)
{
    if (getOpacity() == 1)
    {
        removeFromParentAndCleanup(true);
    }
    else
    {
        setPosition(getPosition() + Vec2(0, -1));
        setOpacity(getOpacity() - 7);
    }
}

ReceivedDamageSprite::ReceivedDamageSprite(int number, cocos2d::Vec2 pos)
{
    _label = Label::createWithSystemFont("-" + helpers::to_string(number), "Arial", 80);
    _label->setTextColor(Color4B::RED);
    _label->setColor(Color3B::RED);
    setAnchorPoint(Vec2(0.5, 0.5));
    this->addChild(_label);
    setPosition(pos);
    setOpacity(255);
}
