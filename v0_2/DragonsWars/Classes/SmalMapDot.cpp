#include "SmalMapDot.h"
#include "reader/CreatorReader.h"

SmalMapDot *SmalMapDot::createObject(DotType type)
{
    auto reader = creator::CreatorReader::createWithFilename("creator/UIElements.ccreator");
    reader->setup();
    auto scene = reader->getSceneGraph(true);

    auto dot = new SmalMapDot();

    std::string path = type == DotType::fire ? "creator/Resources/mapFireDot.png" :
                                                    "creator/Resources/mapFrostDot.png";
    if (dot && dot->initWithFile(path))
        {
            dot->autorelease();
            dot->setContentSize(utils::findChild(scene, "mapFireDot")->getContentSize());

            return dot;
        }

        CC_SAFE_DELETE(dot);
        return nullptr;
}

void SmalMapDot::updatePositionFromPlayerPosition(Vec2 pos, Size bigMapSize)
{
    auto mapSize = this->getParent()->getContentSize();
    float xCoef = mapSize.width / bigMapSize.width;
    float yCoef = mapSize.height / bigMapSize.height;

    this->setPosition(Vec2(pos.x * xCoef, pos.y * yCoef));

}
