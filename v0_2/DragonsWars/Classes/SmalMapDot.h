#pragma once

#include "cocos2d.h"

using namespace cocos2d;

enum DotType { fire, frost, currentPlayer};

class SmalMapDot: public Sprite
{
public:
    static SmalMapDot* createObject(DotType type);

    void updatePositionFromPlayerPosition(Vec2 pos, Size bigMapSize);

private:
    SmalMapDot() = default;
    ~SmalMapDot() = default;
};
