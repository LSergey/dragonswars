#pragma once

#include <tuple>
#include "Bullet.h"

class PlayerData;

class ParamsManager
{
public:
    static ParamsManager* instance();

    std::tuple<int, float, float, float> getSpellBattleParams(PlayerData* playerData, BulletType spellType);
    std::tuple<int, float, float, float> getSpellParams(PlayerData* playerData, BulletType spellType, int level);
    std::tuple<int, int> getSpellUpgradeParams(PlayerData* playerData, BulletType spellType);
private:
    ParamsManager() = default;
    ~ParamsManager() = default;

    static ParamsManager* _instance;
};
