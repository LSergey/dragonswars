#include "Bullet.h"
#include <math.h>
#include "reader/CreatorReader.h"
#include "PhysicsShapeCache.h"
#include "Helpers.h"
#include "RTDataHelper.h"
#include "ParamsManager.h"

Bullet *Bullet::createObject(BulletType type, int bulletLevel, PlayerObject *connectedObject, Sprite *bg)
{
    auto reader = creator::CreatorReader::createWithFilename("creator/BulletsScene.ccreator");
    reader->setup();
    auto scene = reader->getSceneGraph(true);

    auto bullet = new Bullet(type, bulletLevel, connectedObject, bg);

    std::string name;
    if (type == BulletType::fireball)
        name = "fireball";
    else if (type == BulletType::frostball)
        name = "frostball";
    else
        ;

    if (bullet && type != BulletType::dragon_breath && bullet->initWithFile("creator/Resources/" + name + ".png"))
    {
        bullet->autorelease();
        bullet->setContentSize(utils::findChild(scene, name)->getContentSize());
        bullet->setDefaultValues();

        PhysicsBody* body = PhysicsShapeCache::getInstance()->createBodyWithName(/*name*/ "fireball");
        if (!body)
        {
            PhysicsShapeCache::getInstance()->addShapesWithFile("physics.plist", bullet->getScale());
            body = PhysicsShapeCache::getInstance()->createBodyWithName(name);
            CC_ASSERT(body != nullptr);
        }

        bullet->setPhysicsBody(body);
        bullet->schedule(schedule_selector(Bullet::bulletLifetimeEnd), bullet->getLifetime());

        return bullet;
    }
    else if (bullet && type == BulletType::dragon_breath && bullet->init())
    {
        float x1 = sin(helpers::degree_to_radians(connectedObject->getRotation())) * connectedObject->getContentSize().width / 2;
        float y1 = cos(helpers::degree_to_radians(connectedObject->getRotation())) * connectedObject->getContentSize().width / 2;
        bullet->setPosition(connectedObject->getPosition());
        bullet->setRotation(connectedObject->getRotation());
        bullet->exposeBreath(0.f);
        bullet->schedule(schedule_selector(Bullet::exposeBreath), 0.1f);
        bullet->schedule(schedule_selector(Bullet::bulletLifetimeEnd), bullet->getLifetime());
        bullet->setAnchorPoint(Vec2(0, 0));

        return bullet;
    }

        CC_SAFE_DELETE(bullet);
        return nullptr;
}

void Bullet::updatePosition()
{
    if (_bulletType != BulletType::dragon_breath)
    {
        float x1, y1;
        float radians = helpers::degree_to_radians(this->getRotation());

        x1 = this->getPosition().x + sin(radians) * _speedCoeficient;
        y1 = this->getPosition().y + cos(radians) * _speedCoeficient;

        this->setPosition(x1, y1);
    }
    else
    {
        float x1 = sin(helpers::degree_to_radians(_connectedObject->getRotation())) * _connectedObject->getContentSize().width / 2;
        float y1 = cos(helpers::degree_to_radians(_connectedObject->getRotation())) * _connectedObject->getContentSize().width / 2;
        this->setPosition(_connectedObject->getPosition() + Vec2(x1, y1));
        this->setRotation(_connectedObject->getRotation());
    }
}

PlayerData *Bullet::getPlayerData() const
{
    return _connectedObject->getPlayerData();
}

PlayerTeamType Bullet::getPlayerTeamType() const
{
    return _teamType;
}

BulletType Bullet::getBulletType() const
{
    return _bulletType;
}

int Bullet::getBulletDamage() const
{
    return _bulletDamage;
}

int Bullet::getBulletLevel() const
{
    return _bulletLevel;
}

void Bullet::setDefaultValues()
{
    _speedCoeficient = 30;
}

void Bullet::setPlayerTeamType(PlayerTeamType teamType)
{
    _teamType = teamType;
}

float Bullet::getLifetime() const
{
    return _lifetime;
}

bool Bullet::isLifetimeEnded() const
{
    return _lifetimeEnded;
}

void Bullet::exposeBreath(float dt)
{
    if (breathStateNum != 10)
        this->removeChildByName("fb" + helpers::to_string(breathStateNum));

    if (breathStateNum == 10)
        return;
    else
        breathStateNum++;

    std::string path;
    if (_teamType == PlayerTeamType::frost_team)
        path = "frostBreath";
    else
        path = "fireBreath";
    if (breathStateNum != 10)
        path += "0" + helpers::to_string(breathStateNum) + "/" + path + "0" + helpers::to_string(breathStateNum) + "%01d.png";
    else
        path += helpers::to_string(breathStateNum) + "/" + path + helpers::to_string(breathStateNum) + "%01d.png";
    //std::string path = "fireBreath" + helpers::to_string(breathStateNum) + "/%01d.png";

    auto frames = helpers::getAnimation(path.c_str(), 1);
    auto item = Sprite::createWithSpriteFrame(frames.front());
    item->setTag(1);
    item->setAnchorPoint(Vec2(0, 0));
    item->setPosition(Vec2(-item->getContentSize().width / 2, 0));

    auto animation = Animation::createWithSpriteFrames(frames, 1.0f/8);
    item->runAction(RepeatForever::create(Animate::create(animation)));

    std::string name;
    if (_teamType == PlayerTeamType::frost_team)
        name = "frostBreath";
    else
        name = "fireBreath";
    if (breathStateNum != 10)
        name += "0" + helpers::to_string(breathStateNum) + "1";
    else
        name += helpers::to_string(breathStateNum) + "1";
    PhysicsBody* body = PhysicsShapeCache::getInstance()->createBodyWithName(name);
    if (!body)
    {
        PhysicsShapeCache::getInstance()->addShapesWithFile("physics.plist");
        body = PhysicsShapeCache::getInstance()->createBodyWithName(name);
        CC_ASSERT(body != nullptr);
    }

    item->setPhysicsBody(body);

    this->addChild(item, 1, "fb" + helpers::to_string(breathStateNum));
}

void Bullet::bulletLifetimeEnd(float dt)
{
    _lifetimeEnded = true;
}

Bullet::Bullet(BulletType type, int bulletLevel, PlayerObject* connectedObject, Sprite *bg)
    : _bulletType(type),
      _bulletLevel(bulletLevel),
      _connectedObject(connectedObject),
      _bg(bg)
{
    _bulletDamage = std::get<0>(
                ParamsManager::instance()->getSpellBattleParams(_connectedObject->getPlayerData(), _bulletType));
    _lifetime = std::get<3>(
                ParamsManager::instance()->getSpellBattleParams(_connectedObject->getPlayerData(), _bulletType));
}
