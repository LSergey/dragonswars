#include "Helpers.h"

namespace helpers {
float degree_to_radians(float degree)
{
    return ( degree * M_PI ) / 180;
}

Vector<SpriteFrame *> getAnimation(const char *format, int count)
{
    auto spritecache = SpriteFrameCache::getInstance();
    Vector<SpriteFrame*> animFrames;
    char str[100];
    for(int i = 1; i <= count; i++)
    {
        sprintf(str, format, i);
        animFrames.pushBack(spritecache->getSpriteFrameByName(str));
    }
    return animFrames;
}

float radioans_to_degree(float radians)
{
    return (radians * 180) / M_PI;
}

}
