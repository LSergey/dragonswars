#include "SkinsScene.h"
#include "platform/CCFileUtils.h"
#include "reader/CreatorReader.h"
#include "AppDelegate.h"
#include "ui/CocosGUI.h"
#include "Helpers.h"

Scene* SkinsScene::createScene()
{
    SkinsScene *pRet = new(std::nothrow) SkinsScene();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

bool SkinsScene::init() {

    if ( !Scene::init() ) {
        return false;
    }

    auto back_btn = utils::findChild<ui::Button*>(_currentScene, "backBtn");
    back_btn->addClickEventListener([](Ref*) {
        Director::getInstance()->popScene();
    });

    auto scrollView = static_cast<ScrollView*>(utils::findChild(_currentScene, "scrollview"));
    scrollView->setScrollBarEnabled(false);

    for (int i = 0; i < 4; ++i)
    {
        SkinSceneItem* item = SkinSceneItem::createObject(i);
        item->setPosition(Vec2(250, 300 + 700 * i));

        scrollView->getInnerContainer()->addChild(item);
    }

    for (int i = 0; i < 4; ++i)
    {
        SkinSceneItem* item = SkinSceneItem::createObject(i + 4);
        item->setPosition(Vec2(1000, 300 + 700 * i));

        scrollView->getInnerContainer()->addChild(item);
    }

    for (int i = 0; i < 4; ++i)
    {
        SkinSceneItem* item = SkinSceneItem::createObject(i + 8);
        item->setPosition(Vec2(1750, 300 + 700 * i));

        scrollView->getInnerContainer()->addChild(item);
    }
    scrollView->scrollToTop(0, false);

    return true;
}

void SkinsScene::update(float dt)
{

}

Scene *SkinsScene::getScene() const
{
    return _currentScene;
}

SkinsScene::SkinsScene()
{
    auto reader = creator::CreatorReader::createWithFilename("creator/SkinsScene.ccreator");
    reader->setup();
    _currentScene = reader->getSceneGraph();
    addChild(_currentScene);
}

SkinsScene::~SkinsScene()
{

}

SkinSceneItem *SkinSceneItem::createObject(int dragonId)
{
    auto reader = creator::CreatorReader::createWithFilename("creator/DragonsScene.ccreator");
    reader->setup();
    auto scene = reader->getSceneGraph(true);

    auto item = new SkinSceneItem(dragonId);

    std::string imagePath = "res/fireDragon" + helpers::to_string(dragonId) + ".png";
    if (item && item->initWithFile(imagePath))
    {
        item->autorelease();
        item->setContentSize(utils::findChild(scene, "fireDragon")->getContentSize());

        auto button = Button::create("creator/Resources/useDragonBtn.png");
        button->setContentSize(utils::findChild(scene, "useBuyBtn")->getContentSize());
        button->setPosition(utils::findChild(scene, "useBuyBtn")->getPosition());
        auto labelStyled = static_cast<Button*>(utils::findChild(scene, "useBuyBtn"))->getTitleLabel();
        auto label = Label::createWithSystemFont(labelStyled->getString(),
                                          labelStyled->getSystemFontName(),
                                          labelStyled->getSystemFontSize());
        button->setName("btn");
        if (item->isNeedBuy())
            label->setString("Buy \n 100");
        label->setPosition(labelStyled->getPosition());
        button->setTitleLabel(label);

        item->addChild(button);
        button->addTouchEventListener(CC_CALLBACK_2(SkinSceneItem::buttonPresed, item));

        return item;
    }

    CC_SAFE_DELETE(item);
    return nullptr;
}

int SkinSceneItem::getId() const
{
    return _dragonId;
}

bool SkinSceneItem::isNeedBuy() const
{
    return _needBuy;
}

void SkinSceneItem::requestUseResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response)
{
    if (response.GetHasErrors())
    {
    }
    else
    {
        AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->requestPlayerData();
    }
}

void SkinSceneItem::requestBuyResponse(GS &gsInstance, const GameSparks::Api::Responses::LogEventResponse response)
{
    if (response.GetHasErrors())
    {
    }
    else
    {
        AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->requestPlayerData();
        _needBuy = false;
        Button* btn = static_cast<Button*>(getChildByName("btn"));
        auto fontSize = btn->getTitleLabel()->getSystemFontSize();
        btn->setTitleText("Use");
        btn->getTitleLabel()->setSystemFontSize(fontSize);
    }
}

void SkinSceneItem::buttonPresed(Ref *pSender, Widget::TouchEventType type)
{
    if (type == ui::Widget::TouchEventType::ENDED)
    {
        if (!_needBuy)
        {
            GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
            request.SetEventKey("setDragonSkinNumber");
            request.SetEventAttribute("number", this->getId());

            std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
                    std::bind(&SkinSceneItem::requestUseResponse, this, std::placeholders::_1, std::placeholders::_2);
            request.Send(callback, 0);
        }
        else
        {
            GameSparks::Api::Requests::LogEventRequest request(*AppDelegate::sharedAppDelegate()->getGSManager().m_gs.get());
            request.SetEventKey("buySkin");
            request.SetEventAttribute("skinNumber", this->getId());
            request.SetEventAttribute("price", 100);

            std::function<void(GS &, const GameSparks::Api::Responses::LogEventResponse) > callback =
                    std::bind(&SkinSceneItem::requestBuyResponse, this, std::placeholders::_1, std::placeholders::_2);
            request.Send(callback, 0);
        }
    }
}

SkinSceneItem::SkinSceneItem(int dragonId)
    : _dragonId(dragonId)
{
    _needBuy = !(AppDelegate::sharedAppDelegate()->getGSManager().getPlayerDataManager()->getData()->openedSkins[dragonId]);
}
