#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class SplashScene : public cocos2d::Scene
{
public:
    static Scene* createScene();
    
    virtual bool init();
    virtual void update(float dt);

    Scene *getScene() const;

private:
    SplashScene();
    ~SplashScene();

    Scene* _currentScene = nullptr;
    LoadingBar* _loadingBar = nullptr;
    int _tmpCounter = 0;
};
