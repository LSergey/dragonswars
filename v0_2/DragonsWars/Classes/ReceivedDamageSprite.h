#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;

class ReceivedDamageSprite: public Sprite
{
public:
    static ReceivedDamageSprite* createObject(int num, Vec2 pos);

    void update(float dt);

private:
    ReceivedDamageSprite(int number, Vec2 pos);
    ~ReceivedDamageSprite() = default;

    Label* _label;
};
