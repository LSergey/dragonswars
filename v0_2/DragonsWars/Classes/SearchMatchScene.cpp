#include "SearchMatchScene.h"
#include "BaseGameScene.h"
#include "AppDelegate.h"
#include "GSMatchManager.h"
#include "reader/CreatorReader.h"

using namespace cocos2d;

cocos2d::Scene* SearchMatchScene::createScene(GameMode mode)
{
    SearchMatchScene *pRet = new(std::nothrow) SearchMatchScene(mode);
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

bool SearchMatchScene::init()
{
    if ( !Scene::init() )
    {
        return false;
    }

    _loadingBar = static_cast<LoadingBar*>(utils::findChild(_currentScene, "progressBar"));
    joinedPlayersSprites.push_back(static_cast<Sprite*>(utils::findChild(_currentScene, "playerJoined1")));
    joinedPlayersSprites.push_back(static_cast<Sprite*>(utils::findChild(_currentScene, "playerJoined2")));
    joinedPlayersSprites.push_back(static_cast<Sprite*>(utils::findChild(_currentScene, "playerJoined3")));
    joinedPlayersSprites.push_back(static_cast<Sprite*>(utils::findChild(_currentScene, "playerJoined4")));

    joinedPlayersSprites[0]->setVisible(true);
    for (int i = 1; i < joinedPlayersSprites.size(); ++i)
        joinedPlayersSprites[i]->setVisible(false);
    
    this->scheduleUpdate();
    
    return true;
}

void SearchMatchScene::update(float dt)
{
    if (AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getPendingPlayersCount() != 0)
    {
        for (int i = 0; i < AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getPendingPlayersCount(); ++i)
            joinedPlayersSprites[i]->setVisible(true);
    }

    //TODO refactor it when enought data be loaded
    if (AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getStatus() == GSMatchStatus::not_created)
    {
        for (int i = 1; i <= 5; ++i)
            _loadingBar->setPercent(i);
    }
    else if (AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getStatus() == GSMatchStatus::initialising)
    {
        for (int i = 6; i <= 30; ++i)
            _loadingBar->setPercent(i);
    }
    else if (AppDelegate::sharedAppDelegate()->getGSManager().getMatchManager()->getStatus() == GSMatchStatus::initialised
             && _loadingBar->getPercent() < 29.9)
    {
        for (int i = 31; i <= 55; ++i)
            _loadingBar->setPercent(i);

        _tmp_counter++;
    }
    else
    {
        Director::getInstance()->replaceScene(BaseGameScene::createScene(_gameMode));
    }
}

Scene *SearchMatchScene::getScene() const
{
    return _currentScene;
}

SearchMatchScene::SearchMatchScene(GameMode mode)
    : _gameMode(mode)
{
    auto reader = creator::CreatorReader::createWithFilename("creator/SearchMatchScene.ccreator");
    reader->setup();
    _currentScene = reader->getSceneGraph();
    addChild(_currentScene);
}

SearchMatchScene::~SearchMatchScene()
{

}
