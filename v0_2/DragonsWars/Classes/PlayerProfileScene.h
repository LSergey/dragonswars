#pragma once

#include "cocos2d.h"

using namespace cocos2d;

class PlayerProfileScene : public cocos2d::Scene
{
public:

    static Scene* createScene();
    
    virtual bool init();
    
protected:
    PlayerProfileScene();
    ~PlayerProfileScene() = default;

    void setupData();

    Scene* _currentScene = nullptr;
};
