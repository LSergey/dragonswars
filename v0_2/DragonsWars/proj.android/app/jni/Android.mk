LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../Classes)
$(call import-add-path,$(LOCAL_PATH)/../../../Classes/reader)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos/audio/include)


LOCAL_MODULE := MyGame_shared

LOCAL_MODULE_FILENAME := libMyGame

LOCAL_SRC_FILES :=  $(LOCAL_PATH)/hellocpp/main.cpp \
                    $(LOCAL_PATH)/../../../Classes/AnimatedItemWithCollision.cpp \
                    $(LOCAL_PATH)/../../../Classes/AppDelegate.cpp \
                    $(LOCAL_PATH)/../../../Classes/BaseGameScene.cpp \
                    $(LOCAL_PATH)/../../../Classes/BattleResultScene.cpp \
                    $(LOCAL_PATH)/../../../Classes/Bullet.cpp \
                    $(LOCAL_PATH)/../../../Classes/EnterNicknameScene.cpp \
                    $(LOCAL_PATH)/../../../Classes/GameSession.cpp \
                    $(LOCAL_PATH)/../../../Classes/GSAuthManager.cpp \
                    $(LOCAL_PATH)/../../../Classes/GSMainManager.cpp \
                    $(LOCAL_PATH)/../../../Classes/GSMatchManager.cpp \
                    $(LOCAL_PATH)/../../../Classes/Helpers.cpp \
                    $(LOCAL_PATH)/../../../Classes/Joystick.cpp \
                    $(LOCAL_PATH)/../../../Classes/JoystickBase.cpp \
                    $(LOCAL_PATH)/../../../Classes/MainScene.cpp \
                    $(LOCAL_PATH)/../../../Classes/ParamsManager.cpp \
                    $(LOCAL_PATH)/../../../Classes/PhysicsShapeCache.cpp \
                    $(LOCAL_PATH)/../../../Classes/PlayerDataManager.cpp \
                    $(LOCAL_PATH)/../../../Classes/PlayerObject.cpp \
                    $(LOCAL_PATH)/../../../Classes/PlayerProfileScene.cpp \
		    $(LOCAL_PATH)/../../../Classes/ReceivedDamageSprite.cpp \
                    $(LOCAL_PATH)/../../../Classes/RTDataHelper.cpp \
                    $(LOCAL_PATH)/../../../Classes/SearchMatchScene.cpp \
                    $(LOCAL_PATH)/../../../Classes/SkillsScene.cpp \
                    $(LOCAL_PATH)/../../../Classes/SkinsScene.cpp \
                    $(LOCAL_PATH)/../../../Classes/SmalMapDot.cpp \
                    $(LOCAL_PATH)/../../../Classes/SpellSlot.cpp \
                    $(LOCAL_PATH)/../../../Classes/SplashScene.cpp \
                    \
                    $(LOCAL_PATH)/../../../Classes/reader/ \
                    $(LOCAL_PATH)/../../../Classes/reader/animation/AnimateClip.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/animation/AnimationClip.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/animation/AnimationManager.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/animation/Easing.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/animation/Bezier.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/collider/Collider.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/collider/ColliderManager.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/collider/Contract.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/collider/Intersection.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/CreatorReader.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/ui/PageView.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/ui/RichtextStringVisitor.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/animation/Animation.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/animation/AnimationState.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/animation/TimelineState.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/animation/WorldClock.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/armature/Armature.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/armature/Bone.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/armature/Slot.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/core/BaseObject.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/events/EventObject.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/factories/BaseFactory.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/model/AnimationData.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/model/ArmatureData.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/model/DragonBonesData.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/model/FrameData.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/model/TimelineData.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/parsers/DataParser.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/parsers/JSONDataParser.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/textures/TextureData.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/cocos2dx/CCArmatureDisplay.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/cocos2dx/CCFactory.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/cocos2dx/CCSlot.cpp \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones/cocos2dx/CCTextureData.cpp \
                    \
                    $(LOCAL_PATH)/../../../GameSparksSDK/src/GameSparksAll.cpp \

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../Classes \
                    $(LOCAL_PATH)/../../../Classes/reader \
                    $(LOCAL_PATH)/../../../Classes/reader/dragonbones \
                    $(LOCAL_PATH)/../../../GameSparksSDK/include/ \
                    $(LOCAL_PATH)/../../../GameSparksSDK/include/cjson \
                    $(LOCAL_PATH)/../../../GameSparksSDK/include/easywsclient \
                    $(LOCAL_PATH)/../../../GameSparksSDK/include/GameSparks/generated \
                    $(LOCAL_PATH)/../../../GameSparksSDK/include/GameSparks \
                    $(LOCAL_PATH)/../../../GameSparksSDK/include/GameSparksRT \
                    $(LOCAL_PATH)/../../../GameSparksSDK/include/GameSparksRT/Proto \
                    $(LOCAL_PATH)/../../../GameSparksSDK/include/google \
                    $(LOCAL_PATH)/../../../GameSparksSDK/include/mbedtls \
                    $(LOCAL_PATH)/../../../GameSparksSDK/include/System \

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-add-path, $(LOCAL_PATH)/../../../cocos2d)
$(call import-module, cocos)
$(call import-module, reader)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
